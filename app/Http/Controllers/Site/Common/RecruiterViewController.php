<?php

namespace App\Http\Controllers\Site\Common;

use App\Http\Controllers\Controller;
use App\Repositories\RecruiterRepository;

class RecruiterViewController extends Controller
{

    protected $repo;

    public function __construct(RecruiterRepository $repo)
    {
        return $this->repo = $repo;
    }

    public function index()
    {
        $data['recruiters'] = $this->repo->get(request()->all(), 10);
        $data['hideLeftBar'] = 1;

        return view('site.common.recruiters', $data);
    }

    public function show($slug) {
        $data['recruiter'] = $this->repo->bySlug($slug);
        $data['hideLeftBar'] = 1;

        return view('site.common.recruiter_view', $data);
    }
}
