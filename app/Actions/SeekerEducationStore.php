<?php

namespace App\Actions;

use App\Models\UserEducation;

class SeekerEducationStore {

    protected $model;

    public function __construct(UserEducation $model)
    {
        $this->model = $model;
    }

    public function execute(array $data)  {
        return ($this->model->create([
            'user_id' => auth()->user()->id,
            'city_id' => $data['city'],
            'country_id' => $data['country'],
            'degree_id'  => $data['degree'],
            'from' => $data['from'],
            'title' => $data['title'],
            'to' => $data['to'],
            'till' => isset($data['till']) ? 1 : 0
        ])) ? true : false;
    }
}
