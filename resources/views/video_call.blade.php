<html>
<head>
    <title>
        Video Call
    </title>
</head>
<body>

<label for="">Your ID: </label>
<br>
<textarea id="yourId"></textarea>
<br>
<label for="">Other ID: </label>
<br>
<textarea id="otherId"></textarea>
<br>

<button id="connect">
    Connect
</button>

<hr>

<label>Enter Message</label>
<br>
<textarea id="yourMessage"></textarea>
<br>
<br>
<button id="send">Send</button>
<br>
<pre id="messages"></pre>

<script src="{{asset('js/video-call.js')}}"></script>
</body>
</html>
