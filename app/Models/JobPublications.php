<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobPublications extends Model
{
    protected $table = 'job_publications';

    protected $guarded = [];

    public function job() {
        return $this->belongsTo(User::class, 'job_id', 'id');
    }
}
