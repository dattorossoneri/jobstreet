<?php

namespace App\Http\Controllers\Site\Common\Job;

use App\Models\City;
use App\Models\Country;
use App\Models\JobTypes;
use App\Http\Requests\JobRequest;
use App\Repositories\CountryRepository;
use App\Repositories\JobRepository;
use App\Http\Controllers\Controller;

class JobController extends Controller
{
    protected $repo;
    protected $companyRepo;

    public function __construct(JobRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['countries'] = (new CountryRepository())->get();
        $data['jobs'] = (new JobRepository())->get(request()->all(), 10);
        $data['jobTypes'] = JobTypes::get();
        $data['hideLeftBar'] = 1;

        return view('site.common.jobs.index', $data);
    }


    public function show($slug)
    {
        $data['job'] = $this->repo->bySlug($slug);
        $data['recruiter'] = $data['job']->author;

        if(auth()->user()->isSeeker()) {
            $data['sent'] = auth()->user()->sentApplies()->where('job_id', $data['job']->id)->count();
        }

        $data['hideLeftBar'] = 1;

        return view('site.common.jobs.job', $data);
    }

}
