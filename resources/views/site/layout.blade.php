<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang=""><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang=""><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang=""><!--<![endif]-->

@include('site.partials.head')
<body id="board">
@include('site.partials.header')
<main id="wrapper">
    <div class="container bootstrap snippets">
        <div class="row">
            @if(auth()->user() and (empty($hideLeftBar) or !$hideLeftBar))
                <div class="col-lg-3 col-md-3 col-sm-4">
                    <div class="text-center mb-20">
                        <img class="center-block img-responsive" src="{{auth()->user()->image}}" alt="{{auth()->user()->fullName()}}">
                        <h4 class="text-capitalize">{{auth()->user()->fullName()}}</h4>
                        <p class="text-muted text-capitalize mb-20">{{auth()->user()->type()}}</p>
                    </div>

                    <ul class="list-group">
                        @include('site.partials.sidebars.sidebar')
                    </ul>
                </div>
            @endif

            @yield('content')
        </div>
    </div>

</main>

@include('site.partials.footer')

@include('site.partials.scripts')
</body>
</html>
