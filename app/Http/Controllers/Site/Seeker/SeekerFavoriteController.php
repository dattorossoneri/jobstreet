<?php

namespace App\Http\Controllers\Back\Seeker;

use App\Models\UserJobFavorite;
use App\Http\Controllers\Controller;

class SeekerFavoriteController extends Controller
{

    public function index() {

        $data = [];

        return view('back.page.seeker.favorites.index', $data);
    }

    public function loadFavoriteJobs() {
        $data['favorites'] = auth()->user()->favorites;

        return view('back.page.seeker.favorites.loads.favorites', $data);
    }

    public function addOrRemove($id) {

        $response = null;

        if($record = UserJobFavorite::where('job_id',$id)
            ->where('user_id', auth()->user()->id)
            ->first()) {

            $response = 'deleted';

            $record->delete();
        }else{
            UserJobFavorite::create([
                'job_id' => $id,
                'user_id' => auth()->user()->id
            ]);

            $response = 'created';
        }

        return response()->json($response,201);

    }
}
