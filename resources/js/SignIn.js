import axios from 'axios'

$('#signIn').on('submit', async function(e) {
    e.preventDefault()
    const self = $(this)
    const url = self.attr('action')
    const data = self.serialize()

    axios.post(url, data)
        .then(res => {
            location.href = res.data.redirectTo
        })
        .catch(err => {
            alert('error')
        })

});
