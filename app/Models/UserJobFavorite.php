<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserJobFavorite extends Model
{
    protected $table = 'user_job_favorites';

    protected $guarded = [];

    public function job() {
        return $this->belongsTo(Job::class, 'job_id', 'id');
    }
}
