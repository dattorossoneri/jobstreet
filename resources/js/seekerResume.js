import { education, basicInfo, workExperience, portfolio } from './resume'
window.onload = function() {
    $('#basic').click();
};
$(function() {
    const steps = [
        'basic',
        'education',
        'work-experience',
        'portfolio',
        'skills',
        'optional'
    ];

    let step = 'basic';

    const makeActiveStep = $step => {
        const $steps = $('.step');
        $steps.each(function () {
            $(this).parent().removeClass('active');
        });
        const parent = $step.parent();
        parent.addClass('active')
    };

    $('.step').on('click', function (e) {
        e.preventDefault();

        const self = $(this);
        const parent = self.parent();
        const url = parent.data('url');
        step = $(this).data('step');

        if(parent.hasClass('active')) {
            return false;
        }

        makeActiveStep(self);

        if(!steps.includes(step)) {
            return false;
        }

        switch (step) {
            case 'basic':
                basicInfo(url);
                break;
            case 'education':
                education(url);
                break;
            case 'work-experience':
                workExperience(url);
                break;
            case 'portfolio':
                portfolio(url);
                break;
            case 'skills':
                break;
            case 'optional':
                break;
        }

    });
});
