<?php


use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class EnglishLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('english_levels')->truncate();

        $data = [
            [
                'title' => 'No English',
            ],
            [
                'title' => 'Beginner/Elementary',
            ],
            [
                'title' => 'Pre-Intermediate',
            ],
            [
                'title' => 'Intermediate',
            ],
            [
                'title' => 'Upper Intermediate',
            ],
            [
                'title' => 'Advanced/Fluent',
            ]
        ];

        foreach($data as $item) {
            DB::table('english_levels')->insert($item);
        }
    }
}
