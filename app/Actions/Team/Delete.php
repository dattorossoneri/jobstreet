<?php

namespace App\Actions\Team;

use App\Models\Team;

class Delete {

    protected $model;

    public function __construct(Team $model)
    {
        $this->model = $model;
    }

    public function execute(Int $id) {
        $item = $this->model->where('id', $id)->first();

        if($item->creator_id !== auth()->user()->id) {
            abort(403);
        }

        if($item->delete()) {
            return response()->json('deleted', 200);
        } else {
            return response()->json('error', 500);
        }
    }
}
