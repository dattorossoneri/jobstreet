<?php

return [
    'user' => [
        'user_type' => 'user_type_id',
        'country' => 'country_id',
        'city' => 'city_id'
    ],
    'job' => [
        'country' => 'country_id',
        'city' => 'city_id',
        'job_types' => 'job_type_ids'
    ],
    'resume_education' => [
        'degree' => 'degree_id',
        'country' => 'country_id',
        'city' => 'city_id',
    ]
];
