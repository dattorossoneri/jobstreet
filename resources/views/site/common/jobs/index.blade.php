@extends('site.layout')
@section('content')
@push('css')
    <link href="{{ asset('dist/css/tagsinput.css') }}" rel="stylesheet">
    <script src="{{asset('dist/css/select.min.css')}}"></script>
@endpush
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <form action="" method="get">
        <div class="panel panel-default">
            <div class="panel-heading">
                Find Jobs
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" name="keyword" id="keyword" placeholder="Keyword" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <select name="date" id="date" class="form-control no-radius">
                                <option value="">Choose Date</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <select data-live-search="true" data-url="{{route('cities.get',['__id__'])}}" name="country" id="country" class="form-control">
                                <option value="">Choose Country</option>
                                @foreach($countries as $country)
                                    <option {{ (!empty($record) and $record->country_id == $country->id) ? 'selected' : '' }} value="{{$country->id}}">
                                        {{$country->title}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <select data-live-search="true" name="city" id="city" class="form-control">
                                <option value="">Choose City</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <select name="job_types[]" multiple="" id="type" class="form-control no-radius">
                                <option value="">Choose</option>
                                @foreach($jobTypes as $jobType)
                                    <option value="{{$jobType->id}}">
                                        {{$jobType->title}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group mb-0">
                            <input type="text" value="[{{ (!empty($choiceTechnologies)) ? $choiceTechnologies : '' }}]" data-url="{{route('technologies.get')}}" class="form-control" id="technology">
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary">Find Job</button>
            </div>
        </div>
    </form>

    <div class="row">
        <div class="list-group">
            <div class="col-sm-4 col-xs-6 col-md-3 col-lg-2">
                <a class="thumbnail fancybox" rel="ligthbox" href="#">
                    <img class="img-responsive" alt="" src="dist/img/companies/philip_morris.png" />
                    <div class="text-right">
                        <small class="text-muted">Leave CV</small>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-xs-6 col-md-3 col-lg-2">
                <a class="thumbnail fancybox" rel="ligthbox" href="#">
                    <img class="img-responsive" alt="" src="dist/img/companies/philip_morris.png" />
                    <div class="text-right">
                        <small class="text-muted">Leave CV</small>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-xs-6 col-md-3 col-lg-2">
                <a class="thumbnail fancybox" rel="ligthbox" href="#">
                    <img class="img-responsive" alt="" src="dist/img/companies/philip_morris.png" />
                    <div class="text-right">
                        <small class="text-muted">Leave CV</small>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-xs-6 col-md-3 col-lg-2">
                <a class="thumbnail fancybox" rel="ligthbox" href="#">
                    <img class="img-responsive" alt="" src="dist/img/companies/philip_morris.png" />
                    <div class="text-right">
                        <small class="text-muted">Leave CV</small>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-xs-6 col-md-3 col-lg-2">
                <a class="thumbnail fancybox" rel="ligthbox" href="#">
                    <img class="img-responsive" alt="" src="dist/img/companies/philip_morris.png" />
                    <div class="text-right">
                        <small class="text-muted">Leave CV</small>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-xs-6 col-md-3 col-lg-2">
                <a class="thumbnail fancybox" rel="ligthbox" href="#">
                    <img class="img-responsive" alt="" src="dist/img/companies/philip_morris.png" />
                    <div class="text-right">
                        <small class="text-muted">Leave CV</small>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-xs-6 col-md-3 col-lg-2">
                <a class="thumbnail fancybox" rel="ligthbox" href="#">
                    <img class="img-responsive" alt="" src="dist/img/companies/philip_morris.png" />
                    <div class="text-right">
                        <small class="text-muted">Leave CV</small>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-xs-6 col-md-3 col-lg-2">
                <a class="thumbnail fancybox" rel="ligthbox" href="#">
                    <img class="img-responsive" alt="" src="dist/img/companies/philip_morris.png" />
                    <div class="text-right">
                        <small class="text-muted">Leave CV</small>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-xs-6 col-md-3 col-lg-2">
                <a class="thumbnail fancybox" rel="ligthbox" href="#">
                    <img class="img-responsive" alt="" src="dist/img/companies/philip_morris.png" />
                    <div class="text-right">
                        <small class="text-muted">Leave CV</small>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-xs-6 col-md-3 col-lg-2">
                <a class="thumbnail fancybox" rel="ligthbox" href="#">
                    <img class="img-responsive" alt="" src="dist/img/companies/philip_morris.png" />
                    <div class="text-right">
                        <small class="text-muted">Leave CV</small>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-xs-6 col-md-3 col-lg-2">
                <a class="thumbnail fancybox" rel="ligthbox" href="#">
                    <img class="img-responsive" alt="" src="dist/img/companies/philip_morris.png" />
                    <div class="text-right">
                        <small class="text-muted">Leave CV</small>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-xs-6 col-md-3 col-lg-2">
                <a class="thumbnail fancybox" rel="ligthbox" href="#">
                    <img class="img-responsive" alt="" src="dist/img/companies/philip_morris.png" />
                    <div class="text-right">
                        <small class="text-muted">Leave CV</small>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-xs-6 col-md-3 col-lg-2">
                <a class="thumbnail fancybox" rel="ligthbox" href="#">
                    <img class="img-responsive" alt="" src="dist/img/companies/philip_morris.png" />
                    <div class="text-right">
                        <small class="text-muted">Leave CV</small>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-xs-6 col-md-3 col-lg-2">
                <a class="thumbnail fancybox" rel="ligthbox" href="#">
                    <img class="img-responsive" alt="" src="dist/img/companies/philip_morris.png" />
                    <div class="text-right">
                        <small class="text-muted">Leave CV</small>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-xs-6 col-md-3 col-lg-2">
                <a class="thumbnail fancybox" rel="ligthbox" href="#">
                    <img class="img-responsive" alt="" src="dist/img/companies/philip_morris.png" />
                    <div class="text-right">
                        <small class="text-muted">Leave CV</small>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-xs-6 col-md-3 col-lg-2">
                <a class="thumbnail fancybox" rel="ligthbox" href="#">
                    <img class="img-responsive" alt="" src="dist/img/companies/philip_morris.png" />
                    <div class="text-right">
                        <small class="text-muted">Leave CV</small>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-xs-6 col-md-3 col-lg-2">
                <a class="thumbnail fancybox" rel="ligthbox" href="#">
                    <img class="img-responsive" alt="" src="dist/img/companies/philip_morris.png" />
                    <div class="text-right">
                        <small class="text-muted">Leave CV</small>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 col-xs-6 col-md-3 col-lg-2">
                <a class="thumbnail fancybox" rel="ligthbox" href="#">
                    <img class="img-responsive" alt="" src="dist/img/companies/philip_morris.png" />
                    <div class="text-right">
                        <small class="text-muted">Leave CV</small>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div id="no-more-tables">
            <table class="table table-hover" style="margin-bottom: 0">
                <thead class="cf">
                <tr class="active">
                    <th></th>
                    <th>Job Title</th>
                    <th>&nbsp;</th>
                    <th class="numeric">Provided By</th>
                    <th class="numeric">Published</th>
                    <th class="numeric">Deadline</th>
                </tr>
                </thead>
                <tbody>

                @foreach($jobs as $job)
                    <tr>
                        <td data-title="Add in favourites" class="text-center"><a href="#"><i class="fa fa-star-o"></i></a></td>
                        <td data-title="Job Title">
                            <a href="{{route('jobs.show.job',[$job->slug])}}">{{$job->title}}</a>
                        </td>
                        <td data-title="Logo">
                            @if($job->company)
                                <a href="#">
                                    <img src="{{$job->company}}" alt="{{$job->title}}">
                                </a>
                            @else

                            @endif
                        </td>
                        <td data-title="Provided By">
                            <a href="#">
                                @if($job->company)
                                    $job->company->title
                                @else
                                    Recruiter
                                @endif
                            </a>
                        </td>
                        <td data-title="Published">
                            {{ dateForHumans($job->published_at) }}
                        </td>
                        <td data-title="Deadline">
                            {{ dateForHumans($job->deadline) }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="text-center">
        {!! $jobs->appends(request()->all())->links() !!}
    </div>
</div>

@push('js')
    <script src="{{asset('js/tagsinput.js')}}"></script>
    <script src="{{asset('js/jobs.js')}}"></script>
    <script src="{{asset('dist/js/popper.min.js')}}" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="{{asset('dist/js/select.min.js')}}"></script>
@endpush

@endsection
