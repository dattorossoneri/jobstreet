@foreach($offers as $offer)
    <tr>
        <td>
            <a href="javascript:">
                {{$offer->receiver->fullName()}}
            </a>
        </td>
        <td>
            {{ dateForHumans($offer->created_at) }}
        </td>
        <td>
            @include('site.common.partials.offer_buttons',['status' => $offer->status])
        </td>
        <td class="text-center">
            @if($offer->status === 0)
                <button data-action="accept" data-url="{{route('seeker.offer.change_status',[$offer->id])}}" class="btn btn btn-xs btn-success changeStatus">
                    Accept
                </button>
                <button data-action="cancel" data-url="{{route('seeker.offer.change_status', [$offer->id])}}" class="btn btn btn-xs btn-danger changeStatus">
                    Cancel
                </button>
            @endif
        </td>
    </tr>
@endforeach
