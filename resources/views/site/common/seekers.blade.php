@extends('site.layout')
@section('content')
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <form action="user-dashboard.html" method="post">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Job Seeker
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" name="keyword" id="keyword" placeholder="Keyword" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select name="published" id="published" class="form-control">
                                    <option value="">Choose Date</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <select name="country" id="country" class="form-control">
                                    <option value="">Choose Country</option>
                                    <option value="">Choose Country</option>
                                    <option value="">Estonia</option>
                                    <option value="">Latvia</option>
                                    <option value="">Lithuania</option>
                                    <option value="">Kazakhstan</option>
                                    <option value="">Kyrgyzstan</option>
                                    <option value="">Tajikistan</option>
                                    <option value="">Turkmenistan</option>
                                    <option value="">Uzbekistan</option>
                                    <option value="">Belarus</option>
                                    <option value="">Moldova</option>
                                    <option value="">Ukraine</option>
                                    <option value="">Armenia</option>
                                    <option value="">Azerbaijan</option>
                                    <option value="">Georgia</option>
                                    <option value="">Russia</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select name="city" id="city" class="form-control">
                                    <option value="">Choose City</option>
                                    <option value="">Tallinn</option>
                                    <option value="">Tartu</option>
                                    <option value="">Narva</option>
                                    <option value="">Pärnu</option>
                                    <option value="">Kohtla-Järve</option>
                                    <option value="">Viljandi</option>
                                    <option value="">Rakvere</option>
                                    <option value="">Maardu</option>
                                    <option value="">Sillamäe</option>
                                    <option value="">Kuressaare</option>
                                    <option value="">Valga</option>
                                    <option value="">Võru</option>
                                    <option value="">Jõhvi</option>
                                    <option value="">Haapsalu</option>
                                    <option value="">Keila</option>
                                    <option value="">Paide</option>
                                    <option value="">Saue</option>
                                    <option value="">Elva</option>
                                    <option value="">Põlva</option>
                                    <option value="">Tapa</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select name="type" id="type" class="form-control">
                                    <option value="">Job Type</option>
                                    <option value="">Full Time</option>
                                    <option value="">Part Time</option>
                                    <option value="">Remote</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group mb-0">
                                <select name="skills" id="skills" class="form-control">
                                    <option value="">Choose Skills</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-primary">Find Job Seeker</button>
                </div>
            </div>
        </form>

        <div class="panel panel-default">
            <div id="no-more-tables">
                <table class="table table-hover mb-0">
                    <thead class="cf">
                    <tbody>

                    @foreach($seekers as $seeker)
                        <tr>
                            <td data-title="Photo">
                                <a class="image-link" href="{{route('seekers.show',[$seeker->username])}}">
                                    <img class="img-responsive" alt="{{$seeker->fullName()}}" src="{{ $seeker->image }}">
                                </a>
                            </td>
                            <td data-title="About">
                                <h4 class="search-result-item-heading"><a href="{{route('seekers.show',[$seeker->username])}}">
                                        {{ $seeker->fullName() }}
                                    </a></h4>
                                <p class="info">
                                    {{ $seeker->country->title .', '. $seeker->city->title }}
                                </p>
                                <p class="description">
                                    @foreach($seeker->technologies as $technology)
                                        <span>
                                            {{ $technology->title }} ,
                                        </span>
                                    @endforeach
                                </p>
                            </td>
                            <td data-title="Details">
                                <p class="value3 mt-sm">$7</p>
                                <p class="fs-mini text-muted">PER HOUR</p><a class="btn btn-primary btn-info btn-sm" href="{{route('seekers.show',[$seeker->username])}}">View Profile</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="text-center">
            {!! $seekers->appends(request()->all())->links() !!}
        </div>

    </div>
@endsection
