<form id="educationFormStore" action="{{route('seeker.resume.education.store')}}">
    @include('site.seeker.resume.education.partials.form')
</form>

@include('site.seeker.resume.education.partials.scripts')
