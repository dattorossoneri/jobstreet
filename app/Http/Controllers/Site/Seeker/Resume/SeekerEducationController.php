<?php

namespace App\Http\Controllers\Site\Seeker\Resume;

use App\Actions\SeekerEducationUpdate;
use App\Models\Country;
use App\Models\Degree;
use App\Http\Controllers\Controller;
use App\Actions\SeekerEducationStore;
use App\Http\Requests\SeekerEducationRequest;

class SeekerEducationController extends Controller
{

    public function index() {
        return view('site.seeker.resume.education.index');
    }

    public function create() {
        $data['degrees'] = Degree::get();
        $data['countries'] = Country::get();

        return view('site.seeker.resume.education.create', $data);
    }

    public function store(SeekerEducationRequest $request, SeekerEducationStore $action) {
        return response()->json($action->execute($request->all()), 200);
    }

    public function edit($id) {
        $user = auth()->user();
        $data['degrees'] = Degree::get();
        $data['countries'] = Country::get();
        $data['item'] = $user->educations()->where('id',$id)->first();

        return view('site.seeker.resume.education.edit', $data);
    }

    public function update($id, SeekerEducationRequest $request, SeekerEducationUpdate $action) {
        return response()->json($action->execute($id, $request->all()), 200);
    }

    public function list() {
        $data['items'] = auth()->user()->educations()->orderBy('created_at','desc')->get();

        return view('site.seeker.resume.education.list', $data);
    }

    public function delete($id) {
        return auth()->user()->educations()->where('id',$id)->delete();
    }

}
