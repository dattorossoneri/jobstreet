@if(auth()->user()->isRecruiter())
    @include('site.partials.sidebars.recruiter_sidebar')
@else
    @include('site.partials.sidebars.seeker_sidebar')
@endif
