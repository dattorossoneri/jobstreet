<?php

Route::group(['namespace' => 'Api'], function () {
    Route::get('cities/{id}','CityController@index')->name('cities.get');
    Route::get('technologies','TechnologyController@index')->name('technologies.get');
});
