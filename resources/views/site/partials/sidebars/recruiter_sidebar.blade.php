<li class="list-group-item">
    <a class="nav-item " href="{{route('dashboard')}}" class="selected">Dashboard</a>
</li>
<li class="list-group-item">
    <span class="badge">{{$myJobs}}</span>
    <a href="{{route('recruiter_job.index')}}">My Jobs</a>
</li>
<li class="list-group-item">
    <span class="badge">0</span>
    <a href="hr-messages.html">My Messages</a>
</li>
<li class="list-group-item">
    <span class="badge">0</span>
    <a href="hr-activities.html">My Activities</a>
</li>
<li class="list-group-item">
    <span class="badge">{{ auth()->user()->sentOffers()->count() }}</span>
    <a href="{{route('recruiter.offers')}}">My Offers</a>
</li>
<li class="list-group-item">
    <a href="hr-searches.html">My Searches</a>
</li>
