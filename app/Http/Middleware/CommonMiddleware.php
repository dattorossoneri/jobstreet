<?php

namespace App\Http\Middleware;

use Closure;

class CommonMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check() and (auth()->user()->isRecruiter() or auth()->user()->isSeeker()))
            return $next($request);
        else
            return redirect(route('sign_in'));
    }
}
