<?php

namespace App\Actions;

use App\Models\UserEducation;

class SeekerEducationUpdate {

    protected $model;

    public function __construct(UserEducation $model)
    {
        $this->model = $model;
    }

    public function execute($id, array $data)  {
        return $this->model->where('id',$id)->update([
            'city_id' => $data['city'],
            'country_id' => $data['country'],
            'degree_id'  => $data['degree'],
            'from' => $data['from'],
            'title' => $data['title'],
            'to' => $data['to'],
            'till' => isset($data['till']) ? 1 : 0
        ]);
    }
}
