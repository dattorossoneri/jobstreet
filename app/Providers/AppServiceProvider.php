<?php

namespace App\Providers;

use App\Repositories\JobRepository;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view)
        {
            if(auth()->check()) {
                if(auth()->user()->isRecruiter()) {
                    $myJobs = (new JobRepository())->get(
                        ['user_id' => auth()->user()->id],
                        false, true)->count();

                    $view->with('myJobs', $myJobs );
                }
            }
        });

        Schema::defaultStringLength(191);
    }
}
