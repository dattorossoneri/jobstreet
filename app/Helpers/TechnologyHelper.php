<?php

namespace App\Helpers;

use App\Models\Technology;

class TechnologyHelper {
    protected $model;

    public function __construct()
    {
        $this->model = (new Technology());
    }

    public function parse($data) {
        return $this->sanitize($this->decode($data));
    }

    protected function decode($data) {
        $parsed = json_decode($data,true);

        return $parsed;
    }

    protected function sanitize($technologies) {
        $filtered = array_filter($technologies,function($id) {
            if(Technology::find($id)) {
                return $id;
            }
        });

        if(!count($filtered))
            return abort(403);

        return $filtered;
    }
}
