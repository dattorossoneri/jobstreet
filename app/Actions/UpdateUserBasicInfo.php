<?php

namespace App\Actions;

class UpdateUserBasicInfo {
    public function execute(array $data) {
        return auth()->user()->update([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'country_id' => $data['country'],
            'city_id' => $data['city'],
            'phone' => $data['phone'],
            'english_level_id' => $data['english_level'],
            'show_phone' => isset($data['show_phone']) ? 1 : 0
        ]);
    }
}
