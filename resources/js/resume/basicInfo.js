import axios from 'axios'

const basicInfo = url => {
    const $content = $(document).find('#resume-content');
    $content.load(url);

    $(document).on('submit', '#basicInfoForm', async function(e) {
        e.preventDefault();
        const self = $(this);
        const url = self.attr('action');
        const data = self.serialize();
        try {
            const { data: response } = await axios.post(url, data);
            if(response) {
                alert('saved')
            }
        }
        catch(e){
            alert('an error occurred')
        }
    });
};

export default basicInfo;

