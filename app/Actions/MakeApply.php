<?php

namespace App\Actions;

use App\Models\Apply;
use App\Models\Job;

class MakeApply {

    protected $model;

    public function __construct(Apply $model)
    {
        $this->model = $model;
    }

    public function execute(array $data)  {
        $senderId = $this->senderId();
        $jobId = $this->jobId($data['job']);

        sleep(1);

        if(!$this->can($senderId, $jobId)) {
            abort(404);
        }

        return $this->model->create([
            'sender_id' => $senderId,
            'job_id'   => $jobId
        ]);
    }

    protected function can($senderId, $receiverId) {
        $record = $this->model->where([
            'sender_id' => $senderId,
            'job_id' => $receiverId,
        ])->count();

        return (!$record) ? true : false;
    }

    protected function senderId() {
        if(!auth()->user()->isSeeker()) {
            abort(403);
        }

        return auth()->user()->id;
    }

    protected function jobId($id) {
        if( ! Job::find($id) ) {
            abort(403);
        }

        return $id;
    }
}
