<?php
namespace App\Repositories;

use App\Models\User;
use App\Repositories\RepositoryInterface as Repository;

class CommonRepository implements Repository
{
    protected $model;

    // Constructor to bind model to repo
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    // Get all instances of model
    public function get(Array $params = [], $paginate = false, $query = null)
    {
        if(!empty($params)) {
            if( isset($params['keyword']) and $keyword = $params['keyword']) {
                $split = explode(' ', $keyword);
                if(count($split) == 2) {
                    $query->where(function($q) use($split){
                        $q->where('name','LIKE', "%".$split[0]."%");
                        $q->orWhere('surname','LIKE', "%".$split[1]."%");;
                    })->orWhere(function($q) use($split) {
                        $q->where('name','LIKE', "%".$split[1]."%");
                        $q->orWhere('surname','LIKE', "%".$split[0]."%");;
                    });
                }else{
                    $query->where(function($q) use($keyword){
                        $q->where('name','LIKE', "%".$keyword."%");
                        $q->orWhere('surname','LIKE', "%".$keyword."%");;
                    });
                }
            }
        }

        $query->orderBy('id','desc');

        if ($paginate) {
            return $query->paginate($paginate);
        }

        return $this->model->get();
    }

    // create a new record in the database
    public function create(Array $params)
    {

    }

    // update record in the database
    public function update(Array $params, $slug)
    {

    }

    public function bySlug($slug) {
        return $this->model->byUsername($slug);
    }

    // remove record from the database
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    // show the record with the given id
    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }

    public function first($id)
    {
        return $this->model->findOrFail($id);
    }
}
