<?php

namespace App\Actions;


use Carbon\Carbon;

class SetPublications {
    protected $job;

    public function execute($job, $wasPublished, $published) {

        $set = true;

        if($wasPublished == $published) {
            $set = false;
        }

        if($set) {
            $job->publications()->create([
                'date' => Carbon::now(),
                'published' => $published
            ]);
        }
    }
}
