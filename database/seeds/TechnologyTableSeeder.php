<?php

use App\Models\Technology;

use Illuminate\Database\Seeder;

class TechnologyTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('technologies')->truncate();

        $technologies = [
            'Php',
            'Laravel',
            'Symfony',
            'Zend Framework',
            'CodeIgniter',
            'CakePHP',
            'FuelPHP',
            'Yii 2',
            'Phalcon',
            'Slim',
            'PHPixie',
            'Javascript',
            'React.js',
            'Node.js',
            'Angular.js',
            'Nest.js',
            'Express.js',
            'Python',
            'Flask',
            'Django'
        ];

        foreach($technologies as $technology) {
            Technology::create([
                'title' => $technology,
                'slug' => sluggable($technology),
                'position' => 0,
                'hidden' => 0
            ]);
        }
    }
}
