<?php

/*
|--------------------------------------------------------------------------
| JOBSTREET
|--------------------------------------------------------------------------
*/

Route::get('video-call', function() {
    return view('video_call');
});

Route::group(['namespace' => 'Auth' ], function () {
    Route::get('sign-in','LoginController@showLoginForm')->name('sign_in');
    Route::post('sign-in','LoginController@login')->name('sign_in');
    Route::get('sign-up','RegisterController@showRegistrationForm')->name('sign_up');
    Route::post('sign-up','RegisterController@register')->name('sign_up');
    Route::post('/logout', 'LoginController@logout')->name('logout');
});

Route::get('/','Site\Main\MainController@index');

Route::group(['namespace' => 'Site', 'middleware' => 'common'], function () {
    Route::get('/dashboard', 'Dashboard\DashboardController@index')->name('dashboard');

    Route::group(['namespace' => 'Common'], function() {
        Route::group(['namespace' => 'Job', 'prefix' => 'jobs'], function () {
            Route::get('/', 'JobController@index')->name('jobs.index');
            Route::get('/loads','JobController@load')->name('jobs.load.jobs');
            Route::get('/{slug}','JobController@show')->name('jobs.show.job');
            Route::get('/r/{recruiter}','JobController@jobsByRecruiter')->name('jobs.jobs_by_recruiter');
        });

        Route::group(['prefix' => 'seekers'], function() {
            Route::get('/','SeekerViewController@index')->name('seekers.index');
            Route::get('/{slug}','SeekerViewController@show')->name('seekers.show');
        });

        Route::group(['prefix' => 'recruiters'], function() {
            Route::get('/','RecruiterViewController@index')->name('recruiters.index');
            Route::get('/{slug}','RecruiterViewController@show')->name('recruiters.show');
        });

        Route::group(['namespace' => 'Team', 'prefix' => 'team'], function() {
            Route::get('/','TeamController@index')->name('team.index');
            Route::get('/me-teams','TeamController@myTeams')->name('team.my_teams');
            Route::post('/store','TeamController@store')->name('team.create');
            Route::patch('/update','TeamController@update')->name('team.update');
            Route::post('/join', 'TeamController@join')->name('team.join');
            Route::post('/invite', 'TeamController@invite')->name('team.invite');
            Route::delete('/delete/{id}','TeamController@delete')->name('team.delete');
        });
    });

    Route::group(['namespace' => 'Seeker', 'middleware' => ['seeker']], function () {
        Route::group(['prefix' => 'offers'], function () {
            Route::get('/received','SeekerOfferController@index')->name('seeker.offers');
            Route::get('received/load','SeekerOfferController@load')->name('seeker.offers.load');
            Route::post('/change-status/{id}','SeekerOfferController@changeStatus')->name('seeker.offer.change_status');
        });

        Route::group(['prefix' => 'resume', 'namespace'=>'Resume'], function() {
            Route::get('/','SeekerResumeController@index')->name('seeker.resume.index');

            Route::group(['prefix' => 'basic'], function() {
                Route::get('/', 'SeekerBasicInfoController@index')->name('seeker.resume.basic.index');
                Route::post('update', 'SeekerBasicInfoController@update')->name('seeker.resume.basic.update');
            });

            Route::group(['prefix' => 'education'], function() {
                Route::get('/', 'SeekerEducationController@index')->name('seeker.resume.education.index');
                Route::get('/create', 'SeekerEducationController@create')->name('seeker.resume.education.create');
                Route::post('store', 'SeekerEducationController@store')->name('seeker.resume.education.store');
                Route::get('edit/{id}', 'SeekerEducationController@edit')->name('seeker.resume.education.edit');
                Route::post('update/{id}', 'SeekerEducationController@update')->name('seeker.resume.education.update');
                Route::get('list', 'SeekerEducationController@list')->name('seeker.resume.education.list');
                Route::delete('delete/{id}', 'SeekerEducationController@delete')->name('seeker.resume.education.delete');
            });

            Route::group(['prefix' => 'work-experience'], function() {
                Route::get('/', 'SeekerWorkExperienceController@index')->name('seeker.resume.work_experience.index');
                Route::get('/create', 'SeekerWorkExperienceController@create')->name('seeker.resume.work_experience.create');
                Route::post('store', 'SeekerWorkExperienceController@store')->name('seeker.resume.work_experience.store');
                Route::get('edit/{id}', 'SeekerWorkExperienceController@edit')->name('seeker.resume.work_experience.edit');
                Route::post('update/{id}', 'SeekerWorkExperienceController@update')->name('seeker.resume.work_experience.update');
                Route::get('list', 'SeekerWorkExperienceController@list')->name('seeker.resume.work_experience.list');
                Route::delete('delete/{id}', 'SeekerWorkExperienceController@delete')->name('seeker.resume.work_experience.delete');
            });

            Route::group(['prefix' => 'portfolio'], function() {
                Route::get('/', 'SeekerPortfolioController@index')->name('seeker.resume.portfolio.index');
                Route::get('/create', 'SeekerPortfolioController@create')->name('seeker.resume.portfolio.create');
                Route::post('store', 'SeekerPortfolioController@store')->name('seeker.resume.portfolio.store');
                Route::get('edit/{id}', 'SeekerPortfolioController@edit')->name('seeker.resume.portfolio.edit');
                Route::post('update/{id}', 'SeekerPortfolioController@update')->name('seeker.resume.portfolio.update');
                Route::get('list', 'SeekerPortfolioController@list')->name('seeker.resume.portfolio.list');
                Route::delete('delete/{id}', 'SeekerPortfolioController@delete')->name('seeker.resume.portfolio.delete');
            });
        });

        Route::group(['prefix' => 'applies', 'namespace' => 'Apply', 'middleware' => ['seeker']], function() {
            Route::post('/send','ApplyController@send')->name('apply.send');
        });
    });

    Route::group(['namespace' => 'Recruiter', 'middleware' => ['recruiter']], function () {
        Route::group(['prefix' => 'my-jobs'], function () {
            Route::get('/', 'RecruiterJobController@index')->name('recruiter_job.index');
            Route::get('/create', 'RecruiterJobController@create')->name('recruiter_job.create');
            Route::post('/store', 'RecruiterJobController@store')->name('recruiter_job.store');
            Route::get('/edit/{slug}', 'RecruiterJobController@edit')->name('recruiter_job.edit');
            Route::post('/update/{slug}', 'RecruiterJobController@update')->name('recruiter_job.update');
            Route::delete('/delete/{slug}', 'RecruiterJobController@delete')->name('recruiter_job.delete');
        });

        Route::group(['prefix' => 'offers'], function () {
            Route::get('/sent','RecruiterOfferController@index')->name('recruiter.offers');
            Route::get('sent/load','RecruiterOfferController@load')->name('recruiter.offers.load');
            Route::post('/send','RecruiterOfferController@send')->name('recruiter.offer.send');
        });
    });

});
