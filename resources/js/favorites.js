import axios from 'axios'

$(function() {
    (async function() {
        const $div = $('#favoriteJobs');
        const url = $div.data('url');
        const loadJobs = () => {
            $div.load(url);
        }

        loadJobs();
    })();

    (function() {
        $(document).on('click','.favorite',function(e) {
            e.preventDefault();
            const self = $(this);
            const url = self.data('url');
            const job = self.data('job')
            axios.post(url, {}).then ( response => {
                if(response.data == 'created') {
                    self.addClass('saved');
                }else{
                    $(`#favorite_${job}`).remove();
                    self.removeClass('saved');
                }
            }).catch(error => {
                console.log(error);
            })

        });
    })();

});
