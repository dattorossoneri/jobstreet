@extends('site.layout')
@section('content')
    <div class="col-lg-3 col-md-3 col-sm-4">

        <div class="text-center mb-20">
            <img class="center-block img-responsive" src="{{$recruiter->image}}" alt="{{$recruiter->fullName()}}">
            <h4 class="text-capitalize">{{ $recruiter->fullName() }}</h4>
            <p class="text-muted text-capitalize mb-20">{{$recruiter->type()}}</p>
        </div>

        <ul class="list-group">
            <li class="list-group-item">
                <a href="hr-dashboard.html" class="selected">Dashboard</a>
            </li>
            <li class="list-group-item">
                <span class="badge">0</span>
                <a href="hr-jobs.html">Jobs</a>
            </li>
            <li class="list-group-item">
                <span class="badge">0</span>
                <a href="hr-activities.html">Activities</a>
            </li>
            <li class="list-group-item">
                <span class="badge">0</span>
                <a href="hr-activities.html">Comments</a>
            </li>
        </ul>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Feedback for Lika</h3>
            </div>
            <div class="panel-body">
                <p>Evaluate your experience with your employer.</p>
                <div class="radio">
                    <label>
                        <input type="radio" name="optradio">
                        Positive
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="optradio">
                        Neutral
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="optradio">
                        Negative
                    </label>
                </div>
                <div class="form-group">
                    <label for="school">Description</label>
                    <textarea name="" id="" rows="3" class="form-control"></textarea>
                </div>
                <p>
                    <button id="" class="btn btn-primary">
                        Send
                    </button>
                </p>
            </div>
        </div>

    </div>
    <div class="col-lg-9 col-md-9 col-sm-8">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{{ $job->title }}</h3>
            </div>
            <div class="panel-body">
                <h4>Requirements</h4>
                <ul class="list-unstyled">
                    {!! $job->details->requirements !!}
                </ul>

                <h4>Will be plus</h4>
                <ul class="list-unstyled">
                    {!! $job->details->benefits !!}
                </ul>

                <h4>We offer</h4>
                <ul class="list-unstyled">
                    {!! $job->details->we_offer !!}
                </ul>

                <h4>Responsibilities</h4>
                <ul class="list-unstyled">
                    {!! $job->details->responsibilities !!}
                </ul>

                <h4>About Company CUDEV</h4>
                <p>
                    {!! $job->details->about_company !!}
                </p>

                <hr>

                <ul class="list-unstyled mb-0">
                    <li>
                        <strong>Company Website:</strong>
                        <a href="http://www.cudev.com/" target="_blank">http://www.cudev.com/</a>
                    </li>
                    <li><strong>Job publication date:</strong> Sep 27, 2019</li>
                    <li><strong>Views:</strong> 60</li>
                </ul>

            </div>
            @if(auth()->user()->isSeeker())
                <div class="panel-footer">
                    @if(!$sent)
                        <a data-url="{{ route('apply.send') }}" data-id="{{ $job->id }}" id="applyForJob" class="btn btn-primary btn-lg">Apply for job</a>
                        <a disabled="" style="display: none" id="applySent" class="btn btn-primary btn-lg">Apply has been sent</a>
                    @else
                        <a disabled="" id="applySent" class="btn btn-primary btn-lg">Apply has been sent</a>
                    @endif
                </div>
            @endif
        </div>

        <form action="hr-messages.html" method="post">
            <div class="panel panel-default mb-0">
                <div class="panel-heading">
                    <h3 class="panel-title">Send Message</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="description">Job Description</label>
                        <textarea class="form-control" name="description" id="description" rows="5">
    Good afternoon, Lika,

    I am interested in your vacancy, Strong Junior PHP developer.
    http://localhost/terminalio.com/b3-sass/82576-strong-junior-php-developer/
    Let's communicate.
                                    </textarea>
                    </div>
                    <div class="form-group">
                        <label for="title">LinkedIn</label>
                        <input type="text" class="form-control" name="title" id="" value="https://www.linkedin.com/in/leuan/">
                    </div>

                    <hr>
                    <button type="submit" class="btn btn-primary">Start Messaging</button>
                </div>
            </div>
        </form>
    </div>

    @push('js')
        <script src="{{asset('js/Job.js')}}"></script>
    @endpush
@endsection
