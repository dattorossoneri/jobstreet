<?php

namespace App\Helpers;

use App\Models\Job;
use Carbon\Carbon;

class JobFinderHelper {
    protected $model;

    public function __construct()
    {
        $this->model = new Job;
    }

    public function jobsByRecruiterForEmployee() {
        $query =  (new Job())->published();

        if( $value = request()->get('country') ) {

            $query->where('country_id',$value);

            if($value = request()->get('city')) {
                $query->where('city_id',$value);
            }
        }

        if($value = request()->get('keyword')) {
            $query->where(function($q) use($value){
                $q->where('title', "LIKE", "%{$value}%")
                    ->orWhere('description', 'LIKE', "%{$value}%");
            });
        }

        if($values = request()->get('job_types')) {
            $query->whereHas('jobTypes', function($q) use($values) {
                $q->whereIn('job_types_id',$values);
            });
        }

        if($values = request()->get('technologies') and $values !== '[]') {
            $technologies = (new TechnologyHelper())->parse($values);
            $query->whereHas('technologies', function($q) use($technologies) {
                $q->whereIn('technology_id',$technologies);
            });
        }

        if($value = request()->get('job_date')) {
            $date = null;
            switch ($value) {
                case 1:
                    $date = Carbon::now()->addDay(-15);
                break;

                case 2:
                    $date = Carbon::now()->addDay(-7);
                break;

                case 3:
                    $date = Carbon::now()->addDay(-3);
                break;

                case 4:
                    $date = Carbon::now()->addDay(-1);
                break;

                default:
                    break;

//                case 5:
//                    $date = auth()->user()->last_search_date;
//                break;
            }

            $query->whereDate('published_at', '>=', $date);
        }

        return $query->orderBy('created_at')->get();
    }

    public function jobsForEmployeeFeed() {
        $user = auth()->user();
        $technologies = $user->technologies()->pluck('technology_id')->toArray();

        return (new Job())->published()->whereHas('technologies', function($q) use($technologies){
            $q->whereIn('technology_id',$technologies);
        })->orderBy('created_at')->get();

    }
}
