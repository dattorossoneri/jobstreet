<?php

namespace App\Http\Controllers\Auth;

use App\Models\UserType;
use App\Actions\CreateUser;
use App\Http\Controllers\Controller;
use App\Repositories\CountryRepository;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\UserRegistrationRequest;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/Dashboard';

    protected $input = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm() {
        $data['userTypes'] = UserType::get();
        $data['countries'] = (new CountryRepository())->get();

        return view('site.auth.sign_up',$data);
    }

    public function register(UserRegistrationRequest $request, CreateUser $action) {
        $this->input = $request->all();

        if($user = $action->execute($this->input)) {
            auth()->loginUsingId($user->id);

            return response()->json([
                'url' => route('dashboard')
            ],200);
        }
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'title' => fix_typos($this->title),
            'body' => filter_malicious_content($this->body),
            'tags' => convert_comma_separated_values_to_array($this->tags),
            'is_published' => (bool) $this->is_published,
        ]);
    }

}
