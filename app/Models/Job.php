<?php

namespace App\Models;

use App\Models\Traits\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{

    use Sluggable;

    protected $table = 'jobs';

    protected $fillable = ['title','slug','user_id','country_id','city_id','contact_email','published','published_at','deadline'];

    protected $detailFillable = ['job_id','about_company','job_requirements','benefits','we_offer','responsibilities'];

    public function getDetailFillable() {
        return $this->detailFillable;
    }

    public function author() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function details() {
        return $this->hasOne(JobDetail::class, 'job_id');
    }

    public function country() {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function technologies()
    {
        return $this->belongsToMany(Technology::class, 'job_technology');
    }

    public function jobTypes()
    {
        return $this->belongsToMany(JobTypes::class, 'job_types_job');
    }

    public function favorites()
    {
        return $this->hasMany(UserJobFavorite::class, 'job_id')->where('employee_id',auth()->user()->id);
    }

    public function likes()
    {
        return $this->hasMany(JobLikes::class, 'job_id');
    }

    public function views() {
        return $this->hasMany(JobViews::class, 'job_id');
    }

    public function comments() {
        return $this->hasMany(JobComment::class, 'job_id');
    }

    public function company() {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function publications() {
        return $this->hasMany(JobPublications::class, 'job_id');
    }

    public static function scopePublished($query) {
        return $query->where('published',1);
    }

    public function liked($id) {
        return JobLikes::liked($id);
    }

}
