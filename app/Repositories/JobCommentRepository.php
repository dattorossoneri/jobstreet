<?php
namespace App\Repositories;

use App\Models\JobComment;
use App\Repositories\RepositoryInterface as Repository;

class JobCommentRepository implements Repository
{
    protected $model;

    // Constructor to bind model to repo
    public function __construct(JobComment $model)
    {
        $this->model = $model;
    }

    // Get all instances of model
    public function all($paginate = false)
    {
        if ($paginate) {
            return $this->model->paginate($paginate);
        }

        return $this->model->get();
    }

    public function get($id) {
        return JobComment::where([
            'job_id' => $id
        ])->orderBy('id','desc')->get();
    }

    // create a new record in the database
    public function create($request)
    {

    }

    public function addComment($request,$id) {
        if(JobComment::create([
            'user_id' => auth()->user()->id,
            'job_id' => $id,
            'body' => $request->get('body')
        ])) {
            return response()->json('created', 201);
        }else{
            abort(403);
        }
    }

    // update record in the database
    public function update($request, $id)
    {

    }

    // remove record from the database
    public function delete($id)
    {

    }

    protected function deleteImage($company) {

    }

    // show the record with the given id
    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }

    public function first($id)
    {
        return $this->model->findOrFail($id);
    }

}
