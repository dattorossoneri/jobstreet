<form id="educationFormUpdate" action="{{route('seeker.resume.education.update',[$item->id])}}">
    @include('site.seeker.resume.education.partials.form')
</form>

@include('site.seeker.resume.education.partials.scripts')
