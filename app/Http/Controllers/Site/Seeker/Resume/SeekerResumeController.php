<?php

namespace App\Http\Controllers\Site\Seeker\Resume;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SeekerResumeController extends Controller
{
    public function index() {
        $data['hideLeftBar'] = 1;
        return view('site.seeker.resume.index', $data);
    }
}
