const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/SignUp.js', 'public/js')
    .js('resources/js/SignIn.js', 'public/js')
    .js('resources/js/jobs.js','public/js')
    .js('resources/js/job.js','public/js')
    .js('resources/js/seeker.js','public/js')
    .js('resources/js/recruiter.js','public/js')
    .js('resources/js/recruiterJobs.js','public/js')
    .js('resources/js/sluggable.js','public/js')
    .js('resources/js/seekerOffer.js','public/js')
    .js('resources/js/recruiterOffer.js','public/js')
    .js('resources/js/seekerResume.js','public/js')
    .js('resources/js/favorites.js','public/js')
    .js('resources/js/comments.js','public/js')
    .js('resources/js/profile.js','public/js')
    .js('resources/js/likes.js','public/js')
    .js('resources/js/messages.js','public/js')
    .js('resources/js/video-call.js','public/js')
    .js('resources/js/team.js','public/js');

