<?php


use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class UserTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('user_types')->truncate();

        $data = [
            [
                'id' => 1,
                'slug' => 'job-seeker',
                'title' => 'Job Seeker'
            ],
            [
                'id' => 2,
                'slug' => 'recruiter',
                'title' => 'Recruiter'
            ]
        ];

        foreach($data as $item) {
            DB::table('user_types')->insert($item);
        }
    }
}
