<?php

namespace App\DataTransformers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class CreateUserDataTransformer {

    protected $model;

    public function __construct(User $model) {
        $this->model = $model;

    }

    public function execute(array $data) : array {

        $newData = [];

        $transformRules = config('transform_rules')['user'];

        $data['username'] = $this->generateUsername($data['name'], $data['surname']);
        $data['password'] = Hash::make($data['password']);

        foreach($data as $key => $value) {
            if(isset($transformRules[$key]) and $newKey = $transformRules[$key]) {
                $newData[$newKey] = $value;
            }

            if(in_array($key, $this->model->getFillable())) {
                $newData[$key] = $value;
            }
        }

        return $newData;
    }

    protected function generateUsername(string $name, string $surname, int $random = null) : string {
        $username = $name.'-'. $surname;

        if($random) {
            $username .= '-'.$random;
        }

        if($record = $this->model->where('username',$username)->first()) {
            return $this->generateUsername($name, $surname, rand(999,99999));
        }

        return $username;
    }
}
