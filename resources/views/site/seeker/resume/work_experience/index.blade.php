<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Step 3 - Work Experience</div>
            <div class="panel-body" data-url="{{route('seeker.resume.work_experience.create')}}" id="work-experience-content">

            </div>
        </div>
    </div>
    <div data-url="{{route('seeker.resume.work_experience.list')}}" id="work-experience-list">

    </div>
</div>

