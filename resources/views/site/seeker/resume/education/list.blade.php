<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    @if(!$items->isEmpty())
        @foreach($items as $item)
            <table class="table table-bordered">
                <thead>
                <tr>
                    <td colspan="2">{{$item->title}}</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <ul class="list-unstyled">
                            <li>
                                Due from - {{ dateForHumans($item->from) }}
                                <br>
                                @if(!$item->still)
                                    Due to - {{ dateForHumans($item->to) }}
                                @else
                                    Till
                                @endif
                            </li>
                        </ul>
                    </td>
                    <td class="text-center" style="width: 150px;">
                        <a data-url="{{route('seeker.resume.education.edit', [$item->id])}}" class="btn btn-info btn-xs btn-block edit-education">Edit</a>
                        <a data-url="{{route('seeker.resume.education.delete',[$item->id])}}" class="btn btn-danger btn-xs btn-block delete-education">Delete</a>
                    </td>
                </tr>
                </tbody>
            </table>
        @endforeach
    @else
        Education is empty
    @endif
</div>
