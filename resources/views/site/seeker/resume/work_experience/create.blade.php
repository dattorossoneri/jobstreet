<form id="workExperienceFormStore" action="{{route('seeker.resume.work_experience.store')}}">
    @include('site.seeker.resume.work_experience.partials.form')
</form>

@include('site.seeker.resume.work_experience.partials.scripts')
