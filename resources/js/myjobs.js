import axios from 'axios'

(async function() {
    $(document).on('click','.deleteJob', async function(e) {
        e.preventDefault();
        const url = $(this).attr('href');
        if(confirm('Are u sure?')) {
            await axios.delete(url).then(response => {
                if(response.data == 'deleted')
                    location.reload();
            }).catch(err => {
                console.log(err)
            })
        }
    })
})();
