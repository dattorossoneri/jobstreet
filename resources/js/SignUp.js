import axios from 'axios'
import fillCities from "./fill-cities";

fillCities();

$(function() {

    (function() {

        const $errors = $('#errors');

        $('select').selectpicker();

        $('.tab').on('click', function(e){
            $errors.html('').hide();
        });

        $('.terms').on('change', function (e) {
            e.preventDefault();
            if (this.checked) {
                $(`.register`).removeAttr('disabled');
            } else {
                $(`.register`).attr('disabled', 'disabled');
            }
        });

        $('#signUpForm').on('submit', async function(e) {
            e.preventDefault();
            const self = $(this);
            const url = self.attr('action');
            const data = self.serialize();

            await axios.post(url, data).then(response => {
                window.location.href = response.data.url;
            }).catch(error => {

            })
        });
    })();
})
