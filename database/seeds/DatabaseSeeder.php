<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(CountriesTableSeeder::class);
//        $this->call(UserTypesTableSeeder::class);
//        $this->call(UsersTableSeeder::class);
//        $this->call(TechnologyTableSeeder::class);
//        $this->call(CitiesTableSeeder::class);
//        $this->call(JobTypesTableSeeder::class);
//        $this->call(PriceTypesTableSeeder::class);
//        $this->call(SocialNetworksTableSeeder::class);
//        $this->call(DegreesTableSeeder::class);
        $this->call(EnglishLevelTableSeeder::class);
    }
}
