<?php

namespace App\Actions;

use App\Helpers\JobTypeHelper;
use App\Helpers\TechnologyHelper;
use App\Models\Job;
use App\DataTransformers\CreateUpdateJobDataTransformer;
use App\Repositories\JobRepository;

class UpdateJob {

    protected $model;
    protected $repo;
    protected $transformer;

    public function __construct(Job $model, CreateUpdateJobDataTransformer $transformer, JobRepository $repo)
    {
        $this->model = $model;
        $this->repo = $repo;
        $this->transformer = $transformer;
    }

    public function execute($slug, array $data) {

        $record = $this->repo->bySlug($slug);

        $wasPublished = $record->published;

        $data = $this->transformer->execute($data, true, $record);

        $record->update($data['fillable']);

        (new SetPublications())->execute($record, $wasPublished, $data['fillable']['published'] );

        $record->details()->update($data['detail']);

        $technologies = (new TechnologyHelper())->parse($data['additional']['technologies']);
        $jobTypes = (new JobTypeHelper())->sanitize($data['additional']['job_type_ids']);

        $record->technologies()->detach();
        $record->technologies()->attach($technologies);

        $record->jobTypes()->detach();
        $record->jobTypes()->attach($jobTypes);

        return $record;
    }
}
