import axios from 'axios'
import fillCities from '../fill-cities'

fillCities();

const workExperience = url => {
    const $content = $(document).find('#resume-content');
    $content.load(url, function() {
        loadCreate(loadWorkExperiencenList)
    });

    const loadCreate = (cb) => {
        const $content = $(document).find('#work-experience-content');
        $content.load($content.data('url'), function () {
            if(cb)
                cb()
        })
    }

    const loadWorkExperiencenList = (cb) => {
        const $content = $(document).find('#work-experience-list');
        $content.load($content.data('url'), function() {
            if(cb)
                cb();
        })
    }

    const loadEdit = (url, cb) => {
        const $content = $(document).find('#work-experience-content');
        $content.load(url, function() {
            if(cb)
                cb();
        })
    };

    const resetForm = () => {
        $("select").val('default').selectpicker("refresh");
        $('input').val('');
        $('textarea').val('')
    };

    //Edit work experience
    const edit = (function() {
        $(document).on('click','.edit-work-experience', async function(e) {
            e.preventDefault();
            const url = $(this).data('url');
            loadEdit(url)
        });
    })();

    //Store new work experience and reload work experience list
    const update = (function() {
        let submitted = false;
        $(document).on('submit','#workExperienceFormUpdate', async function(e) {
            e.preventDefault();

            if(submitted)
                return false;

            submitted = true;

            const form = $(this);

            try {
                const { data : response } = await axios.post(form.attr('action'), form.serialize());
                if(response)
                    loadWorkExperiencenList(resetForm)
            }
            catch(e) {
                console.log(e)
            }
            finally {
                submitted = false;
            }

        });
    })();

    //Store new work experience and reload work experience list
    const store = (function() {
        let submitted = false;
        $(document).on('submit','#workExperienceFormStore', async function(e) {
            e.preventDefault();

            if(submitted)
                return false;

            submitted = true;

            const form = $(this);

            try {
                const { data : response } = await axios.post(form.attr('action'), form.serialize());
                if(response)
                    loadWorkExperiencenList(resetForm)
            }
            catch(e) {
                console.log(e)
            }
            finally {
                submitted = false;
            }

        });
    })();

    //Delete work experience and reload work experience list
    const remove = (function() {
        let clicked = false;
        $(document).on('click','.delete-work-experience', async function(e) {
            e.preventDefault();
            clicked = true;
            try {
                const url = $(this).data('url');
                const  { data : response } = await axios.delete(url);

                if(response)
                    loadWorkExperiencenList(resetForm())
            }
            catch(e) {
                alert('try again')
            }
            finally {
                clicked = false
            }
        });
    })();
};

export default workExperience;

