<div class="row setup-content" id="step-1">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form action="{{route('seeker.resume.basic.update')}}" id="basicInfoForm">
            <div class="panel panel-primary">
                <div class="panel-heading">Step 1 - Basics</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" value="{{$user->name}}" class="form-control" name="name" id="name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="surname">Surname</label>
                                <input value="{{$user->surname}}" type="text" class="form-control" name="surname" id="surname">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="country">Country</label>
                                <select name="country" id="country" class="form-control">
                                    @foreach($countries as $country)
                                        <option
                                            {{ $country->id === $user->country_id ? 'selected' : '' }}
                                            value="{{$country->id}}">
                                            {{$country->title}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="city">City</label>
                                <select name="city" id="city" class="form-control">
                                    @foreach($cities as $city)
                                        <option
                                            {{ $city->id === $user->city_id ? 'selected' : '' }}
                                            value="{{$city->id}}">
                                            {{$city->title}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone">Phone Number (optional)</label>
                                <input type="text" class="form-control" name="phone" value="{{$user->phone}}" id="phone">
                                <p class="help-block"><i class="fa fa-lock" aria-hidden="true"></i> only provided to employers you apply or respond to.</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">E-Mail Address</label>
                                <input type="email" disabled value="{{$user->email}}" class="form-control" name="email" id="email">
                                <p class="help-block"><i class="fa fa-lock" aria-hidden="true"></i> only provided to employers you apply or respond to.</p>
                            </div>
                        </div>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input {{ $user->show_phone ? 'checked' : '' }} type="checkbox" name="show_phone" value="{{ $user->show_phone ? 1 : 0 }}">Show my phone number on Jobstreet</label>
                    </div>


                    <h5>English Level</h5>
                    <div class="form-group mb-0">
                        @foreach($englishLevels as $englishLevel)
                            <div class="radio">
                                <label>
                                    <input {{$englishLevel->id === $user->english_level_id ? 'checked' : ''}} type="radio" value="{{$englishLevel->id}}" name="english_level">
                                    {{$englishLevel->title}} <span title="{{$englishLevel->title}}">[?]</span>
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <p>
                <button type="submit" class="btn btn-primary">
                    Save <i class="fa fa-arrow-right"></i>
                </button>
            </p>
        </form>
    </div>
</div>
