<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSocialNetwork extends Model
{
    protected $table = 'user_social_network';

    protected $guarded = [];

    public $timestamps = false;
}
