<?php

namespace App\Http\Controllers\Site\Common\Job;

use App\Http\Controllers\Controller;
use App\Http\Requests\JobCommentRequest;
use App\Repositories\JobCommentRepository;

class JobCommentController extends Controller
{
    protected $repo;

    public function __construct(JobCommentRepository $repo)
    {
        $this->repo = $repo;
    }

    public function store(JobCommentRequest $request, $id) {
        $data['comments'] = $this->repo->get($id);

        return $this->repo->addComment($request, $id);

    }

    public function get($id) {
        $data['comments'] = $this->repo->get($id);

        return view('back.page.job_comments.loads.comments', $data);
    }
}
