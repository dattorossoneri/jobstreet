<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="keywords" content="">

    <title>Job Search | Terminalio</title>

    <link rel="icon" type="image/png" href="{{asset('dist/img/icons/favicon.png')}}" />
    <link rel="apple-touch-icon" href="{{asset('dist/img/icons/favicon.png')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway&display=swap">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" href="{{asset('dist/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/styles.css')}}">
    @stack('css')
</head>
