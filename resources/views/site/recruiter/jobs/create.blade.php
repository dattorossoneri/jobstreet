@extends('site.layout')
@section('content')
    <div class="col-lg-9 col-md-9 col-sm-8">
        <form id="jobCreateForm" action="{{route('recruiter_job.store')}}" method="post">
            @include('site.recruiter.jobs.includes.form')
        </form>
    </div>
@endsection
