<?php

namespace App\Helpers;

use App\Models\JobTypes;

class JobTypeHelper {
    protected $model;

    public function __construct()
    {
        $this->model = (new JobTypes());
    }

    public function sanitize($types) {
        $filtered = array_filter($types,function($id) {
            if($this->model->find($id)) {
                return $id;
            }
        });

        if(!count($filtered))
            return abort(403);

        return $filtered;
    }
}
