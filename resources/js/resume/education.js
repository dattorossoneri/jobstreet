import axios from 'axios'
import fillCities from "../fill-cities";
fillCities()

const Education = url => {
    const $content = $(document).find('#resume-content');
    $content.load(url, function() {
        loadCreate(loadEducationList)
    });

    const loadCreate = (cb) => {
        const $content = $(document).find('#education-content');
        $content.load($content.data('url'), function () {
            if(cb)
                cb()
        })
    }

    const loadEducationList = (cb) => {
        const $content = $(document).find('#education-list');
        $content.load($content.data('url'), function() {
            if(cb)
                cb();
        })
    }

    const loadEdit = (url, cb) => {
        const $content = $(document).find('#education-content');
        $content.load(url, function() {
            if(cb)
                cb();
        })
    };

    const resetForm = () => {
        $("select").val('default').selectpicker("refresh");
        $('input').val('');
    };

    //Edit education
    const edit = (function() {
        $(document).on('click','.edit-education', async function(e) {
            e.preventDefault();
            const url = $(this).data('url');
            loadEdit(url)
        });
    })();

    //Store new education and reload education list
    const update = (function() {
        let submitted = false;
        $(document).on('submit','#educationFormUpdate', async function(e) {
            e.preventDefault();
            if(submitted)
                return false;

            submitted = true;

            const form = $(this);

            try {
                const { data : response } = await axios.post(form.attr('action'), form.serialize());
                if(response)
                    loadEducationList(resetForm)
            }
            catch(e) {
                console.log(e)
            }
            finally {
                submitted = false;
            }

        });
    })();

    //Store new education and reload education list
    const store = (function() {
        let submitted = false;
        $(document).on('submit','#educationFormStore', async function(e) {
            e.preventDefault();
            console.log(2)
            if(submitted)
                return false;

            submitted = true;

            const form = $(this);

            try {
                const { data : response } = await axios.post(form.attr('action'), form.serialize());
                if(response)
                    loadEducationList(resetForm)
            }
            catch(e) {
                console.log(e)
            }
            finally {
                submitted = false;
            }

        });
    })();

    //Delete education and reload education list
    const remove = (function() {
        let clicked = false;
        $(document).on('click','.delete-education', async function(e) {
            e.preventDefault();
            clicked = true;
            try {
                const url = $(this).data('url');
                const  { data : response } = await axios.delete(url);

                if(response)
                    loadEducationList(resetForm())
            }
            catch(e) {
                alert('try again')
            }
            finally {
                clicked = false
            }
        });
    })();

};

export default Education;

