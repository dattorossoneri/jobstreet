<?php

namespace App\Http\Controllers\Site\Seeker\Resume;

use App\Actions\SeekerWorkExperienceStore;
use App\Actions\SeekerWorkExperienceUpdate;
use App\Http\Requests\SeekerWorkExperienceRequest;
use App\Models\Country;
use App\Http\Controllers\Controller;

class SeekerWorkExperienceController extends Controller
{

    public function index() {
        return view('site.seeker.resume.work_experience.index');
    }

    public function create() {
        $data['countries'] = Country::get();

        return view('site.seeker.resume.work_experience.create', $data);
    }

    public function store(SeekerWorkExperienceRequest $request, SeekerWorkExperienceStore $action) {
        return response()->json($action->execute($request->all()), 200);
    }

    public function edit($id) {
        $user = auth()->user();
        $data['countries'] = Country::get();
        $data['item'] = $user->workExperience()->where('id',$id)->first();

        return view('site.seeker.resume.work_experience.edit', $data);
    }

    public function update($id, SeekerWorkExperienceRequest $request, SeekerWorkExperienceUpdate $action) {
        return response()->json($action->execute($id, $request->all()), 200);
    }

    public function list() {
        $data['items'] = auth()->user()->workExperience()->orderBy('created_at','desc')->get();

        return view('site.seeker.resume.work_experience.list', $data);
    }

    public function delete($id) {
        return auth()->user()->workExperience()->where('id',$id)->delete();
    }

}
