<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SeekerWorkExperienceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'description'    => 'required|min:10',
            'title'     => 'required|min:2',
            'country'   => 'required',
            'city'      => 'required',
            'from'      => 'required'
        ];

        if( is_null($this->get('till')) ) {
            $rules['to'] = 'required';
        }
        
        return $rules;
    }
}
