<?php

namespace App\Actions;


use App\Models\UserPortfolio;

class SeekerPortfolioUpdate {

    protected $model;

    public function __construct(UserPortfolio $model)
    {
        $this->model = $model;
    }

    public function execute($id, array $data)  {
        return ($this->model->where('id')->update([
            'image' => $data['image'],
            'url' => $data['url'],
            'description' => $data['description']
        ])) ? true : false;
    }
}
