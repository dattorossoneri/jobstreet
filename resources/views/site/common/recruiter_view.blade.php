@extends('site.layout')
@section('content')
    <div class="col-lg-3 col-md-3 col-sm-4">

        <div class="text-center mb-20">
            <img class="center-block img-responsive" src="{{ $recruiter->image }}" alt="{{$recruiter->fullName()}}">
            <h4 class="text-capitalize">{{ $recruiter->fullName() }}</h4>
            <p class="text-muted text-capitalize mb-20">{{ $recruiter->type() }}</p>
        </div>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Open Vacancies</h3>
            </div>
            <div class="panel-body">

                <ul class="list-unstyled mb-0">
                    @foreach($recruiter->jobs->slice(0,5) as $job)
                        <li>
                            <a href="{{route('jobs.show.job', [$job->slug])}}">
                                {{ $job->title }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6">

                <div class="panel panel-default">
                    <div class="panel-heading">

                        <h3 class="panel-title">
                            <div class="pull-left">
                                <img width="35" src="dist/img/recruiters/lika-gogolashvili.jpg" alt="..." class="img-circle">
                                <a href="#">Lika Gogolashvili</a>
                            </div>
                            <div class="pull-right">
                                <span class="text-white h6">Sep 26, 2019</span>
                            </div>
                        </h3>

                        <div class="clearfix"></div>

                    </div>
                    <div class="panel-body">

                        <h5>Strong Junior PHP developer</h5>

                        <p>
                            Knowledge of Laravel, PHP & MySQL. You’ll be
                            working with our in house development team to
                            build a product from the ground up. Free
                            catered lunch on Fridays.
                        </p>

                        <p>
                            <a href="user-dashboard.html" class="btn btn-primary btn-xs">Read more</a>
                        </p>

                        <div class="inner-all block">
                            view all <a href="#">2 comments</a>
                        </div>
                        <div class="line no-margin"></div>
                        <div class="media inner-all no-margin">
                            <div class="pull-left">
                                <img src="dist/img/users/roma_kvikvinia.jpeg" width="50" height="50" alt="..." class="img-post2">
                            </div>
                            <div class="media-body">
                                <a href="user-dashboard.html" class="h4">Roma Kvikvinia</a>
                                <small class="block text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. </small>
                                <em class="text-xs text-muted">Posted on <span class="text-danger">Sep 26, 2019</span></em>
                            </div>
                        </div>
                        <div class="line no-margin"></div>
                        <div class="media inner-all no-margin">
                            <div class="pull-left">
                                <img src="dist/img/users/giorgi_wiklauri.jpg" width="50" height="50" alt="..." class="img-post2">
                            </div>
                            <div class="media-body">
                                <a href="#" class="h4">Giorgi Wiklauri</a>
                                <small class="block text-muted">Quaerat, impedit minus non commodi facere doloribus nemo ea voluptate nesciunt deleniti.</small>
                                <em class="text-xs text-muted">Posted on <span class="text-danger">Sep 26, 2019</span></em>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-md-6">

                <div class="panel panel-default">
                    <div class="panel-heading">

                        <h3 class="panel-title">
                            <div class="pull-left">
                                <img width="35" src="dist/img/recruiters/lika-gogolashvili.jpg" alt="..." class="img-circle">
                                <a href="#">Lika Gogolashvili</a>
                            </div>
                            <div class="pull-right">
                                <span class="text-white h6">Sep 26, 2019</span>
                            </div>
                        </h3>

                        <div class="clearfix"></div>

                    </div>
                    <div class="panel-body">

                        <h4>IT Sales Manager</h4>
                        <p>
                            Good understanding on Object Oriented Programming concepts (Inheritance, Polymorphism, Abstraction and Encapsulation). Good knowledge in Hybrid App development.
                        </p>

                        <p>
                            <a href="#" class="btn btn-primary btn-xs">Read more</a>
                        </p>

                        <div class="inner-all block">
                            view all <a href="#">12 comments</a>
                        </div>
                        <div class="line no-margin"></div>
                        <div class="media inner-all no-margin">
                            <div class="pull-left">
                                <img src="dist/img/users/soso_bichinashvili.jpg" width="50" height="50" alt="..." class="img-post2">
                            </div>
                            <div class="media-body">
                                <a href="#" class="h4">Soso Bichinashvili</a>
                                <small class="block text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. </small>
                                <em class="text-xs text-muted">Posted on <span class="text-danger">Sep 26, 2019</span></em>
                            </div>
                        </div>
                        <div class="line no-margin"></div>
                        <div class="media inner-all no-margin">
                            <div class="pull-left">
                                <img src="dist/img/users/giorgi_bichinashvili.jpg" width="50" height="50" alt="..." class="img-post2">
                            </div>
                            <div class="media-body">
                                <a href="#" class="h4">Giorgi Bichinashvili</a>
                                <small class="block text-muted">Quaerat, impedit minus non commodi facere doloribus nemo ea voluptate nesciunt deleniti.</small>
                                <em class="text-xs text-muted">Posted on <span class="text-danger">Sep 26, 2019</span></em>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
