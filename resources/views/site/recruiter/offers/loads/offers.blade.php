@foreach($offers as $offer)
    <tr>
        <td>
            <a href="javascript:">
                {{$offer->receiver->fullName()}}
            </a>
        </td>
        <td>
            {{ dateForHumans($offer->created_at) }}
        </td>
        <td>
            @include('site.common.partials.offer_buttons',['status' => $offer->status])
        </td>
    </tr>
@endforeach
