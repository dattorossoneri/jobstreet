<?php

namespace App\Actions;

use App\Models\User;
use App\DataTransformers\CreateUserDataTransformer;

class CreateUser {

    protected $model;
    protected $transformer;

    public function __construct(User $model, CreateUserDataTransformer $transformer)
    {
        $this->model = $model;
        $this->transformer = $transformer;
    }

    public function execute(array $data) : User {
        return $this->model->create(
            $this->transformer->execute($data)
        );
    }
}
