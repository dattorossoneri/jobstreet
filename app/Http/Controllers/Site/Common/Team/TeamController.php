<?php

namespace App\Http\Controllers\Site\Common\Team;

use App\Http\Requests\TeamRequest;
use App\Http\Controllers\Controller;
use App\Actions\Team\Create as CreateTeam;
use App\Actions\Team\Delete as DeleteTeam;

class TeamController extends Controller
{
    public function index() {
        $data['hideLeftBar'] = 1;

        return $this->render('index', $data);
    }

    public function myTeams() {
        $data['teams'] = auth()->user()->teams;

        return $this->render('my_team', $data);
    }

    public function create() {
        $data = [];
        return $this->render('create', $data);
    }

    public function store(TeamRequest $request, CreateTeam $action) {
        return $action->execute($request->all());
    }


    public function edit() {
        $data['hideLeftBar'] = 1;
        return $this->render('edit', $data);
    }

    public function update() {

    }

    public function delete($id, DeleteTeam $action) {
        return $action->execute($id);
    }

    protected function render($path, $data) {
        return view("site.common.team.$path", $data);
    }

}
