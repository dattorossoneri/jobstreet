import axios from 'axios'

$(function() {
    (function() {
        let clicked = false;
        $('#applyForJob').on('click', async function(e) {
            e.preventDefault();
            if(clicked)
                return false;

            clicked = true;

            const self = $(this);
            const id = self.data('id');
            const url = self.data('url');
            await axios.post(url, {job: id}).then( response => {
                self.remove();
                $('#applySent').show();
            }).catch(e => {
                console.log(e)
            }).finally( () => {
                clicked = false
            });
        });
    })();
});
