<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserEducation extends Model
{
    protected $table = 'user_education';

    protected $fillable = [
        'user_id','degree_id', 'title', 'country_id', 'city_id', 'from','to','till'
    ];

    public function country() {
        return $this->belongsTo(Country::class, 'country_id');
    }
}
