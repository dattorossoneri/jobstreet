@extends('site.layout')
@section('content')
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <form action="" method="get">
        <div class="panel panel-default">
            <div class="panel-heading">
                Recruiter Search
            </div>
            <div class="panel-body">
                <div class="form-group mb-0">
                    <input type="text" value="{{request()->get('keyword')}}" name="keyword" id="keyword" placeholder="Search" class="form-control">
                </div>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary">Find Recruiter</button>
            </div>
        </div>
    </form>

    <div class="panel panel-default">
        <div id="no-more-tables">
            <table class="table table-hover mb-0">
                <tbody>
                @foreach($recruiters as $recruiter)
                    <tr>
                        <td data-title="Recruiter Photo" class="col-lg-2 col-md-2 col-sm-2 col-xs-12 v-center">
                            <div class="ribbon-wrapper">
                                <div class="ribbon-1 ribbon" title="Verified Recruiter"></div>
                                <a class="image-link" href="{{route('recruiters.show',[$recruiter->username])}}">
                                    <img class="img-responsive" alt="{{$recruiter->fullName()}}" src="{{$recruiter->image}}">
                                </a>
                            </div>
                        </td>
                        <td data-title="Recruiter Info" class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <h4 class="search-result-item-heading">
                                <a href="{{route('recruiters.show',[$recruiter->username])}}">
                                    {{$recruiter->fullName()}}
                                </a>
                            </h4>
                            <p class="description">
                                {{ dateForHumans($recruiter->last_search_date) }}
                            </p>
                        </td>
                        <td data-title="Recruiter Vacancies" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <h6>
                                Open Vacancies
                                <span class="label label-info">View All</span>
                            </h6>
                            <ul class="list-unstyled">
                                @foreach($recruiter->jobs->slice(0,5) as $job)
                                    <li>
                                        <a href="{{ route('jobs.show.job',[$job->slug]) }}">
                                            {{ $job->title }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {!! $recruiters->appends(request()->all())->links() !!}
</div>
@endsection
