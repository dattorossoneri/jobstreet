<?php

namespace App\Http\Controllers\Site\Common;

use App\Http\Controllers\Controller;
use App\Repositories\SeekerRepository;

class SeekerViewController extends Controller
{

    protected $repo;

    public function __construct(SeekerRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index() {
        $data['hideLeftBar'] = 1;
        $data['seekers'] = $this->repo->get(request()->all(),20);

        return view('site.common.seekers', $data);
    }

    public function show($slug) {
        $data['seeker'] = $this->repo->bySlug($slug);

        if(auth()->user()->isRecruiter()) {
            $data['sent'] = auth()->user()->sentOffers()->where('receiver_id', $data['seeker']->id)->count();
        }

        $data['sentOffers'] = auth()->user()->sentOffers()->where('receiver_id', $data['seeker']->id)->get()->pluck('job_id')->toArray();

        $data['hideLeftBar'] = 1;

        return view('site.common.seeker_view', $data);
    }
}
