<?php

namespace App\Models\Traits;

trait Sluggable {
    public function bySlug($slug) {
        return $this->where('slug', $slug)->first();
    }
}
