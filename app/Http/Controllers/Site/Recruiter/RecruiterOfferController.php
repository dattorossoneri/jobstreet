<?php

namespace App\Http\Controllers\Site\Recruiter;

use App\Actions\SendOffer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecruiterOfferController extends Controller
{
    public function index() {
        return view('site.recruiter.offers.index');
    }

    public function load() {
        $data['offers'] = auth()->user()->sentOffers;

        return view('site.recruiter.offers.loads.offers', $data);
    }

    public function send(Request $request, SendOffer $action) {
        return $action->execute($request->all());
    }
}
