<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $table = 'offers';

    protected $fillable = [
        'sender_id', 'receiver_id', 'job_id', 'status'
    ];

    public function sender() {
        return $this->hasOne(User::class, 'id','sender_id');
    }

    public function receiver() {
        return $this->hasOne(User::class, 'id','receiver_id');
    }

    public function job() {
        return $this->hasOne(Job::class, 'id','job_id');
    }
}
