<?php

namespace App\Http\Controllers\Site\Common\Job;

use App\Http\Controllers\Controller;
use App\Models\Job;

class JobLikeController extends Controller
{
    public function store($id) {
        if( (new Job())->liked($id) ) {
            return $this->_unlike($id);
        }else{
            return $this->_like($id);
        }

    }

    protected function _like($id) {
        $job = Job::find($id);
        $job->likes()->create([
            'user_id' => auth()->user()->id
        ]);

        return response()->json('liked', 201);
    }

    protected function _unlike($id) {
        $job = Job::find($id);
        $job->likes()->where('user_id', auth()->user()->id)->delete();
        return response()->json('unliked', 200);
    }
}
