<?php

namespace App\Repositories;

interface RepositoryInterface
{
    /**
     * @param array $params
     * @param bool $paginate
     * @param null $query
     * @return mixed
     */
    public function get(Array $params, $paginate = false, $query = null);

    /**
     * Create new record
     *
     * @param $params Array
     * @return mixed
     */
    public function create(Array $params);

    /**
     * Update or create record.
     *
     * @param $params Array
     * @param $id
     * @return mixed
     */
    public function update(Array $params, $id);

    /**
     * TODO: Soft delete to be implemented
     *
     * @param $id
     * @return mixed
     */
    public function delete($id);

    public function show($id);

    public function first($id);
}
