<?php

use Illuminate\Database\Seeder;

class SocialNetworksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('social_networks')->truncate();

        $data = [
            [
                'title' => 'Facebook',
                'slug' => 'facebook'
            ],
            [
                'title' => 'Twitter',
                'slug' => 'twitter'
            ],
            [
                'title' => 'Linkedin',
                'slug' => 'linkedin'
            ],
            [
                'title' => 'Google',
                'slug' => 'google'
            ],
            [
                'title' => 'Github',
                'slug' => 'github'
            ],
            [
                'title' => 'Skype',
                'slug' => 'skype'
            ],
            [
                'title' => 'Viber',
                'slug' => 'viber'
            ],
            [
                'title' => 'Telegram',
                'slug' => 'telegram'
            ],
            [
                'title' => 'Whatsapp',
                'slug' => 'whatsapp'
            ],
            [
                'title' => 'Wechat',
                'slug' => 'wechat'
            ]
        ];

        foreach($data as $item) {
            \App\Models\SocialNetwork::create([
                'title' => $item['title'],
                'slug' => $item['slug']
            ]);
        }
    }
}
