import axios from  'axios'

$('select').selectpicker();

const Seeker = (function() {
    let clicked = false;
    $('.sendOffer').on('click', function(e) {
        e.preventDefault();
        if(clicked)
            return false;
        clicked = true;

        const self = $(this);
        const receiver = self.data('user');
        const url = self.data('url');
        let jobs = $('#jobs').val();

        if(!jobs) {
            return false;
        }

        jobs = jobs.filter( job => {
           if(job) {
               return job;
           }
        });

        if(jobs.length < 1) {
            return false;
        }

        axios.post(url, { jobs, receiver }).then(() => {
            self.remove();
            $('.offerSent').show();
        }).catch( () => {

        }).finally( () => {
            clicked = false;
        });
    });
});

Seeker();
