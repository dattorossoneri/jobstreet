@push('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    <link href="{{ asset('css/tagsinput.css') }}" rel="stylesheet">
@endpush
{!! csrf_field() !!}
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title pull-left">New Job</h3>
        <div class="pull-right">
            <a href="{{route('recruiter_job.index')}}" class="btn btn-info btn-sm">Go Back</a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">

        <div class="form-group">
            <label for="title">Title</label>
            <input autocomplete="off" type="text" value="{{ (!empty($record)) ? $record->title : '' }}" class="form-control" name="title" id="title">
        </div>
        <div class="form-group">
            <label for="slug">Slug</label>
            <input autocomplete="off" type="text" value="{{ (!empty($record)) ? $record->slug : '' }}" class="form-control" name="slug" id="slug">
            <p class="help-block">Example: frontend-developer</p>
        </div>
        <div class="form-group">
            <label for="email">Contact E-Mail</label>
            <input autocomplete="off" type="email" class="form-control" value="{{ (!empty($record)) ? $record->contact_email : auth()->user()->email }}" name="contact_email" id="email">
        </div>
        <div class="form-group">
            <label for="technologies">Technologies</label>
            <input type="text" value="[{{ (!empty($choiceTechnologies)) ? $choiceTechnologies : '' }}]" data-url="{{route('technologies.get')}}" class="form-control" id="technology">
        </div>

        {{--<div class="form-group">--}}
        {{--<label for="provided_by">Provided By</label>--}}
        {{--<select name="registration_type" id="provided_by" class="form-control">--}}
        {{--<option value="">Choose Company</option>--}}
        {{--<option value="">TBC Bank</option>--}}
        {{--<option value="">Bank of Georgia</option>--}}
        {{--</select>--}}
        {{--</div>--}}

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="country">Country</label>
                    <select data-live-search="true" data-url="{{route('cities.get',['__id__'])}}" name="country" id="country" class="form-control">
                        <option value="">Choose Country</option>
                        @foreach($countries as $country)
                            <option {{ (!empty($record) and $record->country_id == $country->id) ? 'selected' : '' }} value="{{$country->id}}">
                                {{$country->title}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="city">City</label>
                    <select data-live-search="true" name="city" id="city" class="form-control">
                        <option value="">Choose City</option>
                        @if(!empty($record))
                            @foreach($record->country->cities as $city)
                                <option {{ $record->city_id == $city->id ? 'selected' : '' }} value="{{$city->id}}">
                                    {{ $city->title }}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="job_type">Job Type</label>
            <select name="job_types[]" multiple id="job_type" class="form-control">
                <option value="">Choose Job Type</option>
                @foreach($jobTypes as $jobType)
                    <option
                        {{ (!empty($choiceJobTypes) and in_array($jobType->id, $choiceJobTypes) ) ? 'selected' : '' }}
                        value="{{$jobType->id}}">
                        {{$jobType->title}}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="about_company">About Company</label>
            <textarea class="form-control" name="about_company" id="about_company" rows="3">{!! !empty($record) ? $record->details()->first()->about_company : '' !!}</textarea>
        </div>
        <div class="form-group">
            <label for="requirements">Job Requirements</label>
            <textarea class="form-control" name="requirements" id="requirements" rows="3">{!!!empty($record) ? $record->details()->first()->requirements : '' !!}</textarea>
        </div>
        <div class="form-group">
            <label for="benefits">Benefits</label>
            <textarea class="form-control" name="benefits" id="benefits" rows="3">{!! !empty($record) ? $record->details()->first()->benefits : '' !!}</textarea>
        </div>
        <div class="form-group">
            <label for="we_offer">We Offer</label>
            <textarea class="form-control" name="we_offer" id="we_offer" rows="3">{!! !empty($record) ? $record->details()->first()->we_offer : '' !!}</textarea>
        </div>
        <div class="form-group">
            <label for="responsibilities">Responsibilities</label>
            <textarea class="form-control" name="responsibilities" id="responsibilities" rows="3">{!! !empty($record) ? $record->details()->first()->responsibilities : '' !!}</textarea>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="published" {{ (!empty($record) and $record->published) ? 'checked' : '' }}> Published
            </label>
        </div>
        <hr>
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
</div>

@push('js')
    <script src="{{asset('js/tagsinput.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
    <script src="{{asset('js/recruiterJobs.js')}}"></script>
@endpush
