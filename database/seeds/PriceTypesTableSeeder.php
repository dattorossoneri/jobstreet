<?php

use Illuminate\Database\Seeder;

class PriceTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('price_types')->truncate();

        $data = [
            [
                'title' => 'Hourly',
            ],
            [
                'title' => 'Daily'
            ],
            [
                'title' => 'Monthly'
            ],
            [
                'title' => 'Yearly'
            ]
        ];

        foreach($data as $item) {
            \App\Models\PriceType::create([
                'title' => $item['title'],
                'slug' => $item['title']
            ]);
        }
    }
}
