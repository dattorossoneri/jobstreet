import axios from 'axios'
import { sluggable } from "./sluggable";

$(function() {
    $('select').selectpicker();

    (function() {
        let options;
        $('#country').on('change',function() {
            const countryId = $(this).val();
            const url = $(this).data('url').replace('__id__',countryId);
            options = '<option value="">Choose</option>'
            axios.get(url).then( response => {
                for(let city of response.data) {
                    options += `<option value="${city.id}">${city.title}</option>`
                }
                $('#city').html(options).selectpicker('refresh');
            } ).catch( error => {

            });
        });
    })();

    (function() {
        $('#title').on('keyup', function() {
            const slug = sluggable($(this).val());

            $('#slug').val(slug);
        });
    })();

    (function() {
        const $errors = $('#errors');

        const scrollTop = () => {
            window.scrollTo({ top: 0, behavior: 'smooth' });
        }

        function formSubmit($form, successCallBack) {
            axios.post($form.attr('action'), $form.serialize())
                .then( response => successCallBack(response) )
                .catch(error => {
                const errors = error.response.data.errors;
                scrollTop();
                for(let err in errors) {
                    for(let er of errors[err]) {
                        $errors.show().append(`<p>${er}</p>`);
                    }
                }
            })
        }

        $('#jobEditForm').on('submit', function(e) {
            e.preventDefault();
            $errors.html('').hide();
            const $form = $(this);
            const callBack = (response) => {
                const { url } = response.data
                scrollTop()
                window.history.pushState('page2', 'Title', url);
                $form.attr('action',url,)
            }

            formSubmit($form, callBack);
        });

        $('#jobCreateForm').on('submit', function(e) {
            e.preventDefault();
            $errors.html('').hide();
            const $form = $(this);
            const callBack = response => {
                window.location.href = response.data.url;
            }

            formSubmit($form, callBack);
        });

        $(document).on('click','.jobCreate', function(e) {
            e.preventDefault();
            $('#jobCreateForm').submit();
        });

        $(document).on('click', '.jobsUpdate', function(e) {
            e.preventDefault();
            $('#jobEditForm').submit();
        });
    })();

    (async function() {
        const $technology = $('#technology');
        let jsonData = [];
        await axios.get($technology.data('url')).then(response => {
            for(let item of response.data) {
                jsonData.push({
                    'id': item.id,
                    'name': item.title
                })
            }
        }).catch(error => {

        })

        $technology.tagSuggest({
            data: jsonData,
            sortOrder: 'name',
            maxDropHeight: 200,
            name: 'technologies',
            allowFreeEntries: false
        });
    })();
})
