<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserWorkExperience extends Model
{
    protected $table = 'user_work_experience';

    protected $fillable = [
       'title', 'user_id','country_id','city_id','description','from','to','till','company_name'
    ];

    public function country() {
        return $this->belongsTo(Country::class, 'country_id');
    }
}
