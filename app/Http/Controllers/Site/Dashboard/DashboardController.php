<?php

namespace App\Http\Controllers\Site\Dashboard;

use App\Models\Job;
use App\Models\User;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    protected $user;

    public function index()
    {
        $this->user = auth()->user();

        if($this->user->isRecruiter()) {
            return $this->recruiter();
        }elseif($this->user->isSeeker()){
            return $this->seeker();
        }else{
            abort(403);
        }
    }

    protected function recruiter() {
        $data['employers'] = User::recruiters()->count();
        $data['jobs'] = Job::published()->count();
        $data['user'] = $this->user;

        return view('site.recruiter.dashboard', $data);
    }

    protected function seeker() {
        $data['user'] = $this->user;

        return view('site.seeker.dashboard', $data);
    }
}
