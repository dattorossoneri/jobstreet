<?php

namespace App\Http\Controllers\Site\Main;

use App\Http\Controllers\Controller;

class MainController extends Controller
{
    public function index() {
        $data = [];

        return view('site.main.index', $data);
    }
}
