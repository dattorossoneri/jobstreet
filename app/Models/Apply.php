<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Apply extends Model
{
    protected $table = 'applies';

    protected $fillable = [
        'job_id', 'sender_id'
    ];
}
