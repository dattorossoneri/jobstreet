<?php


use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DegreesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('degrees')->truncate();

        $data = [
            [
                'title' => 'High school or equivalent',
            ],
            [
                'title' => 'Associate',
            ],
            [
                'title' => 'Bachelor',
            ],
            [
                'title' => 'Master',
            ],
            [
                'title' => 'Doctorate',
            ],
            [
                'title' => 'Certificate',
            ]
        ];

        foreach($data as $item) {
            DB::table('degrees')->insert($item);
        }
    }
}
