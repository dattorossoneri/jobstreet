<?php

/*
|--------------------------------------------------------------------------
| JOBSTREET
|--------------------------------------------------------------------------
*/

Route::get('video-call', function() {
    return view('video_call');
});

Route::group(['namespace' => 'Auth' ], function () {
    Route::get('sign-in','LoginController@showLoginForm')->name('sign_in');
    Route::post('sign-in','LoginController@login')->name('sign_in');
    Route::get('sign-up','RegisterController@showRegistrationForm')->name('sign_up');
    Route::post('register','RegisterController@register')->name('sign_up');
    Route::post('/logout', 'LoginController@logout')->name('logout');

});

Route::group(['namespace' => 'Back', 'middleware' => ['auth']], function () {
    Route::get('/dashboard', 'Dashboard\DashboardController@index')->name('dashboard');

    Route::group(['prefix' => 'message', 'namespace' => 'Message'], function() {
        Route::get('/', 'MessageController@index')->name('message.index');
        Route::get('get', 'MessageController@get')->name('message.get');
        Route::post('send', 'MessageController@send')->name('message.send');
    });

    Route::group(['prefix' => 'profile'], function() {
        Route::group(['namespace'=>'profile'], function() {
            Route::get('/', 'ProfileController@index')->name('back.profile.index');
            Route::get('edit', 'ProfileController@edit')->name('back.profile.edit');
            Route::post('change-image', 'ProfileController@changeImage')->name('back.profile.change_image');
            Route::post('update-info', 'ProfileController@updateInfo')->name('back.profile.update_info');
            Route::post('update-skills', 'ProfileController@updateSkills')->name('back.profile.update_skills');
            Route::post('update-social-media', 'ProfileController@updateSocialMedia')->name('back.profile.update_social_media');
        });

        Route::group([],function() {
            Route::group(['namespace'=>'Company','prefix' => 'my-companies'], function() {
                Route::get('/', 'CompanyController@index')->name('my_companies.index');
                Route::get('/create', 'CompanyController@create')->name('my_companies.create');
                Route::post('/store', 'CompanyController@store')->name('my_companies.store');
                Route::get('/edit/{slug}', 'CompanyController@edit')->name('my_companies.edit');
                Route::patch('/update/{slug}', 'CompanyController@update')->name('my_companies.update');
                Route::delete('/destroy/{slug}', 'CompanyController@destroy')->name('my_companies.destroy');
            });

            Route::group(['prefix'=>'my-jobs'],function() {
                Route::get('/', 'JobController@index')->name('jobs.index');
                Route::get('/company/{slug}', 'JobController@companyJobs')->name('jobs.company_jobs');
                Route::get('/create/{company?}', 'JobController@create')->name('jobs.create');
                Route::post('/store', 'JobController@store')->name('jobs.store');
                Route::get('/edit/{company?}/{slug}', 'JobController@edit')->name('jobs.edit');
                Route::patch('/update/{slug}', 'JobController@update')->name('jobs.update');
                Route::delete('/destroy/{slug}', 'JobController@destroy')->name('jobs.destroy');
            });
        });

        Route::group(['namespace'=>'Employee'],function() {
            Route::group(['prefix' => 'feed'], function() {
                Route::get('/','FeedController@index')->name('seeker.feed');
                Route::group(['prefix' => 'load'], function() {
                    Route::get('/jobs','FeedController@loadJobs')->name('seeker.load.jobs');
                });
            });

            Route::group(['prefix' => 'favorites'], function() {
                Route::get('/','SeekerFavoriteController@index')->name('seeker.favorite.index');
                Route::post('/{id}','SeekerFavoriteController@addOrRemove')->name('seeker.favorite.add_or_remove');
                Route::group(['prefix' => 'load'], function() {
                    Route::get('/favorite-jobs','SeekerFavoriteController@loadFavoriteJobs')->name('seeker.load.favorite.jobs');
                });
            });

            Route::group(['prefix' => 'recruiter-jobs'], function() {
                Route::get('/', 'RecruiterJobController@index')->name('seeker.recruiter.jobs');
                Route::group(['prefix' => 'load'], function() {
                    Route::get('/jobs','RecruiterJobController@loadJobs')->name('seeker.load.recruiter.jobs');
                });
                Route::get('/{slug}','RecruiterJobController@show')->name('seeker.load.recruiter.job.show');
            });
        });

    });

    Route::group(['prefix' => 'job-comments'], function() {
        Route::post('/store/{id}', 'JobCommentController@store')->name('job_comments.store');
        Route::get('/get/{id}', 'JobCommentController@get')->name('job_comments.get');
    });

    Route::group(['prefix' => 'job-likes'], function() {
        Route::post('/store/{id}', 'JobLikeController@store')->name('job_likes.store');
    });


    Route::get('suggestions', 'SuggestionController@index')->name('back.suggestions');

    Route::get('recruiters', 'RecruiterController@index')->name('back.recruiters');

    Route::get('offers', 'OfferController@index')->name('back.offers');
    Route::get('offers/profile', 'OfferController@profile')->name('back.user_profile');

    Route::get('jobs/{country}', 'JobController@index');
});

