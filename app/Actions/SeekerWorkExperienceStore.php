<?php

namespace App\Actions;

use App\Models\UserWorkExperience;

class SeekerWorkExperienceStore {

    protected $model;

    public function __construct(UserWorkExperience $model)
    {
        $this->model = $model;
    }

    public function execute(array $data)  {
        return ($this->model->create([
            'user_id' => auth()->user()->id,
            'city_id' => $data['city'],
            'country_id' => $data['country'],
            'description' => $data['description'],
            'company_name' => $data['company'],
            'from' => $data['from'],
            'title' => $data['title'],
            'to' => $data['to'],
            'till' => isset($data['till']) ? 1 : 0
        ])) ? true : false;
    }
}
