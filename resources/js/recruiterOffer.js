$(function () {
    (function() {
        const loadOffers = () => {
            const el = $('#offers');
            const url = el.data('url');
            el.load(url)
        };

        loadOffers();
    })();
});
