@extends('site.layout')
@section('content')
    @push('css')
        <style>
            .error-text {
                font-style: italic;
                color: red;
                font-size: 12px;
            }
        </style>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    @endpush
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{route('dashboard')}}">Dashboard</a></li>
                <li class="active">Teams</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <form action="{{route('team.create')}}" id="createTeam" method="post">
                {{csrf_field()}}
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        New Team
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="team_title">Team Name</label>
                            <input type="text" placeholder="Team name" class="form-control" name="title" id="team_title" value="" >

                        </div>
                        <div class="form-group">
                            <label for="team_description">Team Description</label>
                            <textarea name="description" placeholder="Team description" id="team_description" cols="30" rows="2" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="team_member">Team Members</label>
                            <select class="form-control" id="team_member" multiple="multiple">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="team_message">Give this message a personal touch.</label>
                            <textarea name="message" id="team_message" cols="30" rows="2" class="form-control" placeholder="I'd like to invite you to join test on Terminalio. We use Terminalio to organize tasks, projects, due dates, and much more."></textarea>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-primary">Create a Team</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    My Teams
                </div>
                <div data-url="{{route('team.my_teams')}}" id="my-teams">

                </div>
            </div>
        </div>
    </div>
    @push('js')
        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
        <script src="{{asset('js/team.js')}}"></script>
    @endpush
@endsection
