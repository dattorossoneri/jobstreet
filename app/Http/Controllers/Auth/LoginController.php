<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\SignInRequest;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

        $this->redirectTo = route('dashboard');
    }

    public function showLoginForm() {
        $data = [];
        return view('site.auth.sign_in', $data);
    }

    public function login(SignInRequest $request) {

        if($record = User::where('email', $request->get('email'))->first()) {
            if( Hash::check( $request->get('password'), $record->password  ) ) {
                auth()->loginUsingId($record->id);
                return response()->json([
                    'redirectTo' => $this->redirectTo
                ], 200);
            }
        }

        return response()->json([
            'msg' => 'Invalid Credintials'
        ],500);
    }

    public function logout() {
        auth()->logout();
        return redirect('/');
    }
}
