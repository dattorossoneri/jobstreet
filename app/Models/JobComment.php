<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobComment extends Model
{
    protected $table = 'job_comments';

    protected $guarded = [];

    public function job() {
        return $this->belongsTo(Job::class,'job_id');
    }

    public function author() {
        return $this->belongsTo(User::class,'user_id');
    }
}
