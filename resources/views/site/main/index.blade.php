@extends('site.layout')
@section('content')
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Georgia
                </div>
                <a href="view-all-jobs.html">
                    <img src="{{asset('dist/img/city/pic1.jpg')}}" class="img-responsive" alt="">
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Ukraine
                </div>
                <img src="{{asset('dist/img/city/pic1.jpg')}}" class="img-responsive" alt="">
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Lithuania
                </div>
                <img src="{{asset('dist/img/city/pic1.jpg')}}" class="img-responsive" alt="">
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Latvia
                </div>
                <img src="{{asset('dist/img/city/pic1.jpg')}}" class="img-responsive" alt="">
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Estonia
                </div>
                <img src="{{asset('dist/img/city/pic1.jpg')}}" class="img-responsive" alt="">
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Finland
                </div>
                <img src="{{asset('dist/img/city/pic1.jpg')}}" class="img-responsive" alt="">
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Switzerland
                </div>
                <img src="{{asset('dist/img/city/pic1.jpg')}}" class="img-responsive" alt="">
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Poland
                </div>
                <img src="{{asset('dist/img/city/pic1.jpg')}}" class="img-responsive" alt="">
            </div>
        </div>
    </div>
@endsection
