<header>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{route('dashboard')}}">
                    Terminalio
                </a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                @if(auth()->check())
                    <ul class="nav navbar-nav">
                        <li><a href="{{route('jobs.index')}}">Jobs</a></li>
                        <li><a href="{{route('recruiters.index')}}">Recruiters</a></li>
                        <li><a href="{{route('seekers.index')}}">Seekers</a></li>
                    </ul>
                @endif
                <ul class="nav navbar-nav navbar-right">
                    @if(!auth()->check())
                        <li class="active"><a href="{{route('sign_in')}}">Sign In</a></li>
                        <li><a href="{{route('sign_up')}}">Sign Up</a></li>
                    @endif
                    <li class="dropdown">
                        <a href="#"
                           class="dropdown-toggle"
                           data-toggle="dropdown"
                           role="button"
                           aria-haspopup="true"
                           aria-expanded="false">English <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">English</a></li>
                            <li><a href="#">Русский</a></li>
                            <li><a href="#">Українська</a></li>
                            <li><a href="#">ქართული</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

</header>
