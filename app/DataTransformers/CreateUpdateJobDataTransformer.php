<?php

namespace App\DataTransformers;

use App\Models\Job;
use Carbon\Carbon;

class CreateUpdateJobDataTransformer {

    protected $model;

    public function __construct(Job $model) {
        $this->model = $model;
    }

    public function execute(array $data, Bool $update = false, $job = null) : array {

        $newData = [];

        $transformRules = config('transform_rules')['job'];

        if(!$update) {
            $data['slug'] = $this->generateSlug($data['title']);
        }else{
            if($job->slug != $data['slug']) {
                $data['slug'] = $this->generateSlug($data['title']);
            }
        }

        $data['user_id'] = auth()->user()->id;

        $data['published'] = isset($data['published']) ? 1 : 0;

        if($data['published']) {
            $data['published_at'] = Carbon::now();
            $data['deadline'] = Carbon::now()->addMonth(1);
        }

        foreach($data as $key => $value) {
            if(isset($transformRules[$key]) and $newKey = $transformRules[$key]) {
                $key = $newKey;
            }

            if(in_array($key, $this->model->getFillable())) {
                $newData['fillable'][$key] = $value;
            }
            elseif(in_array($key, $this->model->getDetailFillable())) {
                $newData['detail'][$key] = $value;
            }else{
                $newData['additional'][$key] = $value;
            }
        }


        return $newData;
    }

    protected function generateSlug(string $title, int $random = null) : string {
        $slug = sluggable($title);

        if($random) {
            $slug .= '-'.$random;
        }

        if($record = $this->model->where('slug',$slug)->first()) {
            return $this->generateSlug($title, rand(999,99999));
        }

        return $slug;
    }
}
