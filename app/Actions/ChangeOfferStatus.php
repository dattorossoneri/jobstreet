<?php

namespace App\Actions;

use App\Models\Offer;

class ChangeOfferStatus {

    public function execute($offerId, $status)  {
        $status = $this->validateStatus($status);
        $offer = $this->validateOffer($offerId);
        return [
            'updated' => $offer->update([
                'status' => $status
            ])
        ];
    }

    protected function validateOffer($id) {
        if(! $offer = Offer::findOrFail($id) or $offer->receiver_id !== auth()->user()->id or $offer->status > 0) {
            abort(403);
        }

        return $offer;
    }

    protected function validateStatus($status) {
        if(!in_array($status, [0,1,2])) {
            abort(403);
        }

        return $status;
    }

}
