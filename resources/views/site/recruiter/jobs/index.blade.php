@extends('site.layout')
@section('content')
    <div class="col-lg-9 col-md-9 col-sm-8">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title pull-left">My Jobs</h3>
                <span class="pull-right">
                    <a href="{{route('recruiter_job.create')}}" class="btn btn-success btn-sm">
                        New Job
                    </a>
                </span>
                <div class="clearfix"></div>
            </div>
            <table class="table table-bordered table-hover">
                <thead>
                <tr class="active">
                    <th>Title</th>
                    <th>Provided By</th>
                    <th>Published</th>
                    <th>Deadline</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($jobs as $job)
                    <tr>
                        <td><a href="">{{$job->title}}</a></td>
                        <td>{{ ($job->company) ? $job->company->title : 'No company' }}</td>
                        <td>{{ ($job->published) ? dateForHumans($job->published_at) : "Not published" }}</td>
                        <td>{{ ($job->published) ? dateForHumans($job->deadline) : 'Not published' }}</td>
                        <td style="text-align: center">
                            <a class="btn-sm btn-primary" href="{{route('recruiter_job.edit', $job->slug)}}">
                                Edit
                            </a>
                            &nbsp;
                            &nbsp;
                            <a class="btn-sm btn-danger deleteJob" href="{{route('recruiter_job.delete', $job->slug)}}">
                                Delete
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {!! $jobs->links() !!}

    </div>

    @push('js')
        <script src="{{asset('js/myJobs.js')}}"></script>
    @endpush
@endsection
