import axios from 'axios'
import FormData from 'form-data'

$(function() {
    (function() {
        $('#changePassword').on('click', function(e){
            e.preventDefault();
            $('#passwordInputs').toggle();
        })
    })();

    //Upload profile image
    (function () {
        $('#changeImage').on('change', function(e) {
            e.preventDefault();
            const self = $(this);
            const url = self.data('url');
            const file = self[0].files[0];
            const config = {
                'accept': 'application/json',
                'Accept-Language': 'en-US,en;q=0.8',
                'Content-Type': `multipart/form-data`,
            };

            let data = new FormData();
            data.append('file', file, file.name);
            axios.post(url,data, config).then(response => {
                $('.profileImage').attr('src',response.data.image);
            }).catch(error => {
                console.log(error);
            })
        })
    })();

    (function() {
        const workExperience =  $(".workExperience").slider();;
    })();

    (function() {
        $('#skillsForm').on('submit', async function(e) {
            e.preventDefault();
            const self = $(this);
            const url = self.attr('action');
            const data = self.serialize();

            await axios.post(url, data).then(response => {
                console.log(response.data);
            }).catch(err => {

            })
        });
    })();

    (async function() {
        $('#socialNetworksForm').on('submit', async function(e) {
            e.preventDefault();
            const self = $(this);
            const url = self.attr('action');
            const data = self.serialize();
            await axios.post(url,data).then(response => {
                console.log(response);
            }).catch(err => {
                console.log(err);
            });
        })
    })();
});
