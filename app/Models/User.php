<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'username',
        'email',
        'email_verified_at',
        'password',
        'phone',
        'active',
        'user_type_id',
        'english_level_id',
        'country_id',
        'city_id',
        'image',
        'last_search_date',
        'show_phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function companies() {
        return $this->hasMany(Company::class,'user_id');
    }

    public function teams() {
        return $this->hasMany(Team::class,'creator_id')->orderBy('id','desc');
    }

    public function educations() {
        return $this->hasMany(UserEducation::class,'user_id');
    }

    public function workExperience() {
        return $this->hasMany(UserWorkExperience::class,'user_id');
    }

    public function portfolio() {
        return $this->hasMany(UserPortfolio::class,'user_id');
    }

    public function jobs() {
        return $this->hasMany(Job::class, 'user_id');
    }

    public function country() {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function englishLevel() {
        return $this->belongsTo(EnglishLevel::class, 'english_level_id', 'id');
    }

    public function city() {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function userType() {
        return $this->belongsTo(UserType::class, 'user_type_id', 'id');
    }

    public function technologies()
    {
        return $this->belongsToMany(Technology::class, 'user_technology','user_id');
    }

    public function prices()
    {
        return $this->hasMany(UserWorkPrice::class, 'user_id');
    }

    public function socialNetworks()
    {
        return $this->hasMany(UserSocialNetwork::class, 'user_id');
    }

    public function favorites()
    {
        return $this->hasMany(UserJobFavorite::class, 'user_id');
    }

    public function sentOffers() {
        return $this->hasMany(Offer::class, 'sender_id');
    }

    public function receivedOffers() {
        return $this->hasMany(Offer::class, 'receiver_id');
    }

    public function sentApplies() {
        return $this->hasMany(Apply::class, 'sender_id');
    }

    public function comments() {
        return $this->hasMany(JobComment::class, 'user_id');
    }

    public static function scopeRecruiters($query) {
        return $query->where('user_type_id',2);
    }

    public static function scopeSeekers($query) {
        return $query->where('user_type_id',1);
    }

    public function fullName() {
        return $this->name .' '.$this->surname;
    }

    public function isSeeker() {
        return $this->user_type_id === 1;
    }

    public function isRecruiter() {
        return $this->user_type_id === 2;
    }

    public function type() {
        return $this->userType->title;
    }

    public function byUsername($username) {
        return $this->where('username', $username)->first();
    }
}
