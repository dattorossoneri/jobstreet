<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Step 4 - Portfolio</div>
            <div class="panel-body" data-url="{{route('seeker.resume.portfolio.create')}}" id="portfolio-content">

            </div>
        </div>
    </div>
    <div data-url="{{route('seeker.resume.portfolio.list')}}" id="portfolio-list">

    </div>
</div>

