import axios from "axios";

const fillCities = () => {
    (() => {
        let options = '';
        $(document).on('change','#country', function() {
            const countryId = $(this).val();

            let url = $(this).data('url');
            url = url.replace('__id__',countryId);
            options = '<option value="">Choose</option>';

            axios.get(url).then( response => {
                for(let city of response.data) {
                    options += `<option value="${city.id}">${city.title}</option>`
                }
                $(document).find('#city').html(options).selectpicker('refresh');
            } ).catch( error => {

            });
        });
    })()
}

export default fillCities
