<div class="form-group">
    <label for="title">Title</label>
    <input type="text" autocomplete="off" value="{{ (!empty($item)) ? $item->title : '' }}" class="form-control" name="title" id="title">
</div>
<div class="form-group">
    <label for="url">Url</label>
    <input type="text" value="{{ (!empty($item)) ? $item->url : '' }}" autocomplete="off" class="form-control" name="url" id="url">
</div>
<div class="form-group">
    <label for="image">Image</label>
    <input type="file" class="form-control" name="image" id="image">
</div>
<div class="form-group">
    <label for="description">Description</label>
    <textarea name="" id="description" cols="30" rows="3" class="form-control">{!! (!empty($item) ? $item->description : '') !!}</textarea>
</div>

<button type="submit" class="btn btn-success btn-sm">
    <i class="fa fa-save"></i> Save
</button>
