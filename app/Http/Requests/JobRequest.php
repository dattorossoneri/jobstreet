<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->merge(['slug' =>  sluggable($this->get('slug')) ]);

        return [
            'title' => 'required|min:2',
            'slug' => 'required|min:4',
            'technologies' => 'required',
            'country' => 'required|integer|exists:countries,id',
            'city' => 'required|integer|exists:cities,id',
            'contact_email' => 'required|email',
            'job_types' => 'required'
        ];
    }
}
