import axios from 'axios'
import { sluggable } from "./sluggable";

$(function() {
    $('select').selectpicker();

    (function() {
        let options;
        $('#country').on('change',function() {
            const countryId = $(this).val();
            const url = $(this).data('url').replace('__id__',countryId);
            options = '<option value="">Choose</option>'
            axios.get(url).then( response => {
                for(let city of response.data) {
                    options += `<option value="${city.id}">${city.title}</option>`
                }
                $('#city').html(options).selectpicker('refresh');
            } ).catch( error => {

            });
        });
    })();


    (async function() {
        const $technology = $('#technology');
        let jsonData = [];
        await axios.get($technology.data('url')).then(response => {
            for(let item of response.data) {
                jsonData.push({
                    'id': item.id,
                    'name': item.title
                })
            }
        }).catch(error => {

        })

        $technology.tagSuggest({
            data: jsonData,
            sortOrder: 'name',
            maxDropHeight: 200,
            name: 'technologies',
            allowFreeEntries: false
        });
    })();
});
