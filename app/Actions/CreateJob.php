<?php

namespace App\Actions;

use App\Models\Job;
use App\Helpers\JobTypeHelper;
use App\Helpers\TechnologyHelper;
use App\DataTransformers\CreateUpdateJobDataTransformer;

class CreateJob {

    protected $model;
    protected $transformer;

    public function __construct(Job $model, CreateUpdateJobDataTransformer $transformer)
    {
        $this->model = $model;
        $this->transformer = $transformer;
    }

    public function execute(array $data) : Job {
        $data = $this->transformer->execute($data);

        $record = $this->model->create($data['fillable']);
        $record->details()->create($data['detail']);

        (new SetPublications())->execute($record, false, $data['fillable']['published'] );

        $technologies = (new TechnologyHelper())->parse($data['additional']['technologies']);
        $jobTypes = (new JobTypeHelper())->sanitize($data['additional']['job_type_ids']);


        $record->technologies()->attach($technologies);
        $record->jobTypes()->attach($jobTypes);

        return $record;
    }
}
