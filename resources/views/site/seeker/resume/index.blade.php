@extends('site.layout')
@section('content')
    @push('css')
        <link rel="stylesheet" href="{{asset('dist/css/select.min.css')}}">
        <link rel="stylesheet" href="{{asset('dist/css/datepicker.min.css')}}">
    @endpush
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <ol class="breadcrumb">
            <li><a href="{{route('dashboard')}}">Dashboard</a></li>
            <li class="active">My Resume</li>
        </ol>

        <div class="row form-group">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="nav nav-pills nav-justified thumbnail setup-panel">
                    <li data-url="{{route('seeker.resume.basic.index')}}" >
                        <a class="step" id="basic" data-step="basic">
                            <h6 class="list-group-item-heading">Basics</h6>
                            <p class="list-group-item-text">Step 1 of 6</p>
                        </a>
                    </li>
                    <li data-url="{{route('seeker.resume.education.index')}}">
                        <a class="step" data-step="education">
                            <h6 class="list-group-item-heading">Education</h6>
                            <p class="list-group-item-text">Step 2 of 6</p>
                        </a>
                    </li>
                    <li data-url="{{route('seeker.resume.work_experience.index')}}" >
                        <a class="step" data-step="work-experience">
                            <h6 class="list-group-item-heading">Work Experience</h6>
                            <p class="list-group-item-text">Step 3 of 6</p>
                        </a>
                    </li>
                    <li data-url="{{route('seeker.resume.portfolio.index')}}" >
                        <a class="step" data-step="portfolio">
                            <h6 class="list-group-item-heading">Portfolio</h6>
                            <p class="list-group-item-text">Step 4 of 6</p>
                        </a>
                    </li>
                    <li>
                        <a class="step" data-step="skills">
                            <h6 class="list-group-item-heading">Skills</h6>
                            <p class="list-group-item-text">Step 5 of 6</p>
                        </a>
                    </li>
                    <li>
                        <a class="step" data-step="optional">
                            <h6 class="list-group-item-heading">Optional</h6>
                            <p class="list-group-item-text">Step 6 of 6</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row setup-content">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div id="resume-content" class="row">
                </div>
            </div>
        </div>
    </div>

    @push('js')
        <script src="{{asset('dist/js/popper.min.js')}}"></script>
        <script src="{{asset('dist/js/select.min.js')}}"></script>
        <script src="{{asset('dist/js/datepicker.min.js')}}"></script>
        <script src="{{asset('js/seekerResume.js')}}"></script>
    @endpush
@endsection
