import axios from 'axios'
$(function () {
    (function() {
        const loadOffers = () => {
            const el = $('#offers');
            const url = el.data('url');
            el.load(url)
        };

        loadOffers();

        //Change offer status
        let clicked = false;
        let data = {};
        $(document).on('click','.changeStatus', function(e) {
            e.preventDefault();

            if(clicked)
                return false;

            clicked = true;

            const self = $(this);
            const action = self.data('action');
            const url = self.data('url');

            if(action === 'accept') {
                data.status = 1;
            }else if(action === 'cancel') {
                data.status = 2;
            }else {
                return false;
            }

            axios.post(url, data).then( response => {
                loadOffers();
            } ).catch(e => {
                console.log(e)
            }). finally( () => {
                clicked = false;
            })

        });
    })();
});
