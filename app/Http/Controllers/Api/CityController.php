<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Country;

class CityController extends Controller
{
	public function index($id) {
        return response()->json(
            Country::find($id)->cities
        ,200);
    }
}
