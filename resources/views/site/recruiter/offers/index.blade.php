@extends('site.layout')
@section('content')
    <div class="col-lg-9 col-md-9 col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">My Offers</h3>
            </div>
            <table class="table table-bordered table-hover">
                <thead>
                <tr class="active">
                    <th>Receiver</th>
                    <th>Status</th>
                    <th>Send Date</th>
                </tr>
                </thead>
                <tbody data-url="{{route('recruiter.offers.load')}}" id="offers">

                </tbody>
            </table>
        </div>
    </div>

    @push('js')
        <script src="{{asset('js/recruiterOffer.js')}}"></script>
    @endpush
@endsection
