import axios from 'axios'

(function () {
    $('#messageSend').on('click', async function(e) {
        e.preventDefault();
        const self = $(this);
        const id = self.data('id');
        const text = $('#text').val();
        const url = self.data('url');

        await axios.post(url, {id,text}).then(response => {
            loadMessage();
        }).catch(err => {
            console.log(err);
        })
    });

    const $messages = $('#messages');
    const loadMessage =  () => {
        $messages.load($messages.data('url'));
    };

    loadMessage();

    setInterval(loadMessage, 5000);

})();
