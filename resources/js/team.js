import axios from 'axios'

$(function() {
    const findElByName = (searchAreal, name) => {
        const input = searchAreal.find(`input[name=${name}]`);
        const textarea = searchAreal.find(`textarea[name=${name}]`);
        const select = searchAreal.find(`select[name=${name}]`);

        return input.length ? input : textarea.length ? textarea : select;
    };

    const drawMyTeams = () => {
        const div = $('#my-teams');
        div.load(div.data('url'))
    };

    drawMyTeams();

    const form = $('#createTeam');
    let clicked = false;
    form.on('submit', async function(e) {
        e.preventDefault();
        if(clicked)
            return false;
        clicked = true;
        $('.error-text').remove();
        const self = $(this);
        try {
            const { data:msg } = await axios.post(self.attr('action'), self.serialize());
            if(msg === 'created') {
                drawMyTeams()
            }
        } catch({response}) {
            const {errors} = response.data;
            for(let elName in errors) {
                const el = findElByName(self, elName);
                for(let error of errors[elName]) {
                    const errorSpan = `<span class="error-text">${error}</span>`;
                    el.parent().append(errorSpan)
                }
            }
        } finally {
            clicked = false;
        }
    });

    $(document).on('click', '.delete-team', function(e) {
        e.preventDefault();
        const self = $(this);
        if(confirm('Are u sure ?')) {
            axios.delete(self.data('url')).then(({data: msg}) => {
                if(msg === 'deleted')
                    drawMyTeams();
            }).catch(err => {
                console.log(err)
            })
        }
    });

    $(document).on('change', '#team_member', function(e) {
        e.preventDefault();
        console.log(e.target.value)
    });


    $('#team_member').select2({
        createTag: function (params) {
            var term = $.trim(params.term);

            if (term === '') {
                return null;
            }

            return {
                id: term,
                text: term,
                newTag: true // add additional parameters
            }
        }
    });
});
