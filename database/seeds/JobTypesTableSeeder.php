<?php

use Illuminate\Database\Seeder;

class JobTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('job_types')->truncate();

        $data = [
            ['title' => 'Remote'],
            ['title' => 'Part-time'],
            ['title' => 'Full-time'],
        ];

        foreach($data as $item) {
            DB::table('job_types')->insert([
                'title' => $item['title'],
                'slug' => sluggable($item['title'])
            ]);
        }
    }
}
