<?php

namespace App\Actions\Team;

use App\Models\Team;

class Create {

    protected $model;

    public function __construct(Team $model)
    {
        $this->model = $model;
    }

    public function execute(array $data) {
        $created = $this->model->create([
            'creator_id' => auth()->user()->id,
            'title' => $data['title'],
            'description' => $data['description'],
            'message' => $data['message']
        ]);

        if($created) {
            return response()->json('created', 200);
        } else {
            return response()->json('error', 500);
        }
    }
}
