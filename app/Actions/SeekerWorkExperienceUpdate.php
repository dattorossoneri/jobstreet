<?php

namespace App\Actions;

use App\Models\UserWorkExperience;

class SeekerWorkExperienceUpdate {

    protected $model;

    public function __construct(UserWorkExperience $model)
    {
        $this->model = $model;
    }

    public function execute($id, array $data)  {
        return $this->model->where('id',$id)->update([
            'city_id' => $data['city'],
            'country_id' => $data['country'],
            'description' => $data['description'],
            'company_name' => $data['company'],
            'from' => $data['from'],
            'title' => $data['title'],
            'to' => $data['to'],
            'till' => isset($data['till']) ? 1 : 0
        ]);
    }
}
