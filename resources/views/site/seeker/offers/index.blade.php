@extends('site.layout')
@section('content')
    <div class="col-lg-9 col-md-9 col-sm-8">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">My Offers</h3>
            </div>
            <table class="table table-bordered table-hover">
                <thead>
                <tr class="active">
                    <th>Sender</th>
                    <th>Send Date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody data-url="{{route('seeker.offers.load')}}" id="offers">

                </tbody>
            </table>
        </div>
    </div>

    @push('js')
        <script src="{{asset('js/seekerOffer.js')}}"></script>
    @endpush
@endsection
