@extends('site.layout')
@section('content')
    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
        <div class="row">
            <div class="col-md-6">

                <div class="panel panel-default">
                    <div class="panel-heading">

                        <h3 class="panel-title">
                            <div class="pull-left">
                                <img width="35" src="dist/img/recruiters/lika-gogolashvili.jpg" alt="..." class="img-circle">
                                <a href="#">Lika Gogolashvili</a>
                            </div>
                            <div class="pull-right">
                                <a href="#" class="text-white h5">
                                    <i class="fa fa-heart"></i> 10K
                                </a>
                            </div>
                        </h3>

                        <div class="clearfix"></div>

                    </div>
                    <div class="panel-body">

                        <h5>Laravel Web Developer</h5>
                        <p>
                            <span class="text-white h6">Sep 19, 2019</span>
                        </p>

                        <p>
                            Knowledge of Laravel, PHP & MySQL. You’ll be
                            working with our in house development team to
                            build a product from the ground up. Free
                            catered lunch on Fridays.
                        </p>

                        <p>
                            <a href="user-dashboard.html" class="btn btn-primary btn-xs">Read more</a>
                        </p>

                    </div>
                    <div class="panel-footer">

                        <h1 class="panel-title">
                            <div class="pull-left">
                                <i class="fa fa-eye" aria-hidden="true"></i> 1450
                            </div>
                            <div class="pull-right">
                                <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 430
                            </div>
                        </h1>

                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>
            <div class="col-md-6">

                <div class="panel panel-default">
                    <div class="panel-heading">

                        <h3 class="panel-title">
                            <div class="pull-left">
                                <img width="35" src="dist/img/recruiters/tamuna_kobakhidze.jpeg" alt="..." class="img-circle">
                                <a href="#">Tamuna Kobakhidze</a>
                            </div>
                            <div class="pull-right">
                                <a href="#" class="text-white h5">
                                    <i class="fa fa-heart"></i> 14K
                                </a>
                            </div>
                        </h3>

                        <div class="clearfix"></div>

                    </div>
                    <div class="panel-body">

                        <h5>Senior React Native Developer</h5>

                        <p>
                            <span class="text-white h6">Sep 27, 2019</span>
                        </p>

                        <p>
                            Good understanding on Object Oriented Programming
                            concepts (Inheritance, Polymorphism, Abstraction
                            and Encapsulation). Good knowledge in Hybrid
                            App development.
                        </p>

                        <p>
                            <a href="user-dashboard.html" class="btn btn-primary btn-xs">Read more</a>
                        </p>

                    </div>
                    <div class="panel-footer">

                        <h1 class="panel-title">
                            <div class="pull-left">
                                <i class="fa fa-eye" aria-hidden="true"></i> 230
                            </div>
                            <div class="pull-right">
                                <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 130
                            </div>
                        </h1>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
