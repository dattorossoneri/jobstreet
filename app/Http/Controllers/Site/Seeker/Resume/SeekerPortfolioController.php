<?php

namespace App\Http\Controllers\Site\Seeker\Resume;

use App\Models\Country;
use App\Http\Controllers\Controller;
use App\Actions\SeekerPortfolioStore;
use App\Actions\SeekerPortfolioUpdate;
use App\Http\Requests\SeekerPortfolioRequest;


class SeekerPortfolioController extends Controller
{

    public function index() {
        return view('site.seeker.resume.portfolio.index');
    }

    public function create() {
        $data['countries'] = Country::get();

        return view('site.seeker.resume.portfolio.create', $data);
    }

    public function store(SeekerPortfolioRequest $request, SeekerPortfolioStore $action) {
        return response()->json($action->execute($request->all()), 200);
    }

    public function edit($id) {
        $user = auth()->user();
        $data['countries'] = Country::get();
        $data['item'] = $user->portfolio()->where('id',$id)->first();

        return view('site.seeker.resume.portfolio.edit', $data);
    }

    public function update($id, SeekerPortfolioRequest $request, SeekerPortfolioUpdate $action) {
        return response()->json($action->execute($id, $request->all()), 200);
    }

    public function list() {
        $data['items'] = auth()->user()->portfolio()->orderBy('created_at','desc')->get();

        return view('site.seeker.resume.portfolio.list', $data);
    }

    public function delete($id) {
        return auth()->user()->portfolio()->where('id',$id)->delete();
    }

}
