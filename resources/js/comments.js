import axios from 'axios'

$(function() {
    (async function() {
        const loadComments = () => {
            const $comments = $('#comments');
            const url = $comments.data('url');
            $comments.load(url);
        };

        loadComments();

        const $body = $('#comment_body');

        $('#commentForm').on('submit', async function(e) {
            e.preventDefault();
            const self = $(this);
            const url = self.attr('action');
            const data = self.serialize();
            await axios.post(url, data).then( response => {
                if(response.data == 'created')
                    loadComments();
            }).catch(error => {

            }).finally(() => {
                $body.val('');
            })
        })

        $body.on('keyup', function() {
            const value = this.value;
            if(value.includes('@')) {

            }
        });

    })();

});
