<form id="portfolioFormUpdate" action="{{route('seeker.resume.portfolio.update',[$item->id])}}">
    @include('site.seeker.resume.portfolio.partials.form')
</form>

@include('site.seeker.resume.portfolio.partials.scripts')
