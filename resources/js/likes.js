import axios from 'axios'

$(function() {
    (async function() {
        $('.like').on('click', async function(e) {
            e.preventDefault();
            const self = $(this);
            const $countedLikes= $('.countedLikes')
            let countedLikes = parseInt($countedLikes.text());

            const url = self.data('url');
            await axios.post(url, {}).then(response => {
                if(response.data === 'liked') {
                    self.addClass('liked');
                    $('.likedSpan').show();
                    $('.notLikedSpan').hide();
                    countedLikes += 1;
                }else{
                    self.removeClass('liked');
                    $('.likedSpan').hide();
                    $('.notLikedSpan').show();
                    countedLikes -= 1;
                }
            }).catch(err => {
                console.log(err);
            })

            $countedLikes.text(countedLikes);
        })
    })();

});
