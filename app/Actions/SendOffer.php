<?php

namespace App\Actions;

use App\Models\Offer;
use App\Models\User;

class SendOffer {

    protected $model;

    public function __construct(Offer $model)
    {
        $this->model = $model;
    }

    public function execute(array $data)  {
        $senderId = $this->validateSenderId();
        $receiverId = $this->validateReceiverId($data['receiver']);
        $jobIds = $this->validateJobIds($data['jobs']);

        if(!$this->can($receiverId, $jobIds)) {
            abort(404);
        }

        sleep(1);

        foreach($jobIds as $jobId) {
            $this->model->create([
                'sender_id' => $senderId,
                'receiver_id'   => $receiverId,
                'job_id' => $jobId
            ]);
        }
    }

    protected function can($receiverId, $jobIds) {
        $user = auth()->user();
        $sentOffers = $user->sentOffers()->where('receiver_id', $receiverId)->get()->pluck('job_id')->toArray();

        foreach($jobIds as $jobId) {
            if( in_array($jobId, $sentOffers) ) {
                return false;
            }
        }

        return true;
    }

    protected function validateJobIds($ids) {
        $ownJobs = auth()->user()->jobs->pluck('id')->toArray();

        foreach($ids as $id) {
            if( !in_array($id, $ownJobs)) {
                abort(404);
            }
        }

        return $ids;
    }

    protected function validateSenderId() {
        if(!auth()->user()->isRecruiter()) {
            abort(403);
        }

        return auth()->user()->id;
    }

    protected function validateReceiverId($id) {
        if( ! $user = User::find($id) or !$user->isSeeker()) {
            abort(403);
        }

        return $id;
    }
}
