@extends('site.layout')
@section('content')
    @push('css')
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    @endpush
    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
        <ul class="list-group">
            <li class="list-group-item">
                <a href="user-dashboard.html" class="selected">Dashboard</a>
            </li>

            <li class="list-group-item">
                <span class="badge">1</span>
                <a href="user-messages.html">My Messages</a>
            </li>
            <li class="list-group-item">
                <span class="badge">1</span>
                <a href="user-offers.html">My Offers</a>
            </li>
            <li class="list-group-item">
                <span class="badge">2</span>
                <a href="user-favourites.html">My Favourites</a>
            </li>
            <li class="list-group-item">
                <span class="badge">1</span>
                <a href="user-projects.html">My Bids</a>
            </li>
            <li class="list-group-item">
                <a href="user-resume.html">My Resume</a>
            </li>
        </ul>

        @if(auth()->user()->isRecruiter())
            <li class="list-group-item">
                <button type="button" class="btn btn-primary btn-lg btn-block mb-20" data-toggle="modal" data-target="#myOffer">
                    Send Offer
                </button>
            </li>
        @endif

        <div class="modal fade" id="myOffer" tabindex="-1" role="dialog" aria-labelledby="myOfferLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myOfferLabel">My Offer</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group mb-0">
                            <label for="job">Select Multiple</label>
                            <select name="jobs[]" data-live-search="true" id="jobs" class="form-control selectpikcer" multiple>
                                <option disabled value="">
                                    Choose
                                </option>
                               @foreach(auth()->user()->jobs as $job)
                                    <option  {{ (in_array($job->id, $sentOffers) ? '' : '' ) }} value="{{$job->id}}">{{$job->title}}</option>
                               @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" data-user="{{ $seeker->id }}" data-url="{{ route('recruiter.offer.send') }}" class="btn btn-primary sendOffer">Send</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('js')
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
        <script src="{{asset('js/seeker.js')}}"></script>
    @endpush
@endsection
