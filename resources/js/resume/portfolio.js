import axios from 'axios'
import formData from 'form-data'

const Portfolio = url => {
    const $content = $(document).find('#resume-content');
    $content.load(url, function() {
        loadCreate(loadPortfolioList)
    });

    const loadCreate = (cb) => {
        const $content = $(document).find('#portfolio-content');
        $content.load($content.data('url'), function () {
            if(cb)
                cb()
        })
    }

    const loadPortfolioList = (cb) => {
        const $content = $(document).find('#portfolio-list');
        $content.load($content.data('url'), function() {
            if(cb)
                cb();
        })
    }

    const loadEdit = (url, cb) => {
        const $content = $(document).find('#portfolio-content');
        $content.load(url, function() {
            if(cb)
                cb();
        })
    };

    const resetForm = () => {
        $("select").val('default').selectpicker("refresh");
        $('input').val('');
    };

    //Edit portfolio
    const edit = (function() {
        $(document).on('click','.edit-portfolio', async function(e) {
            e.preventDefault();
            const url = $(this).data('url');
            loadEdit(url)
        });
    })();

    //Update portfolio and reload portfolio list
    const update = (function() {
        let submitted = false;
        $(document).on('submit','#portfolioFormUpdate', async function(e) {
            e.preventDefault();
            if(submitted)
                return false;

            submitted = true;

            const form = $(this);

            try {
                const { data : response } = await axios.post(form.attr('action'), form.serialize());
                if(response)
                    loadPortfolioList(resetForm)
            }
            catch(e) {
                console.log(e)
            }
            finally {
                submitted = false;
            }

        });
    })();

    //Store new portfolio and reload portfolio list
    const store = (function() {
        let submitted = false;
        $(document).on('submit','#portfolioFormStore', async function(e) {
            e.preventDefault();
            if(submitted)
                return false;

            submitted = true;

            const form = $(this);

            try {
                let data = new FormData();
                const $files = $(this).find('input[type=file]');
                const image = ($files[0].files.length) ? $files[0].files[0] : '';
                data.append('title',$(this).find('#title').val());
                data.append('url',$(this).find('#url').val());
                data.append('image',image);
                data.append('description',$(this).find('#description').val());

                const config = {
                    headers: { 'content-type': 'multipart/form-data' }
                };

                const { data : response } = await axios.post(form.attr('action'), data, config);

                if(response)
                    loadPortfolioList(resetForm)
            }
            catch(e) {
                console.log(e)
            }
            finally {
                submitted = false;
            }

        });
    })();

    //Delete portfolio and reload portfolio list
    const remove = (function() {
        let clicked = false;
        $(document).on('click','.delete-portfolio', async function(e) {
            e.preventDefault();
            clicked = true;
            try {
                const url = $(this).data('url');
                const  { data : response } = await axios.delete(url);

                if(response)
                    loadPortfolioList(resetForm())
            }
            catch(e) {
                alert('try again')
            }
            finally {
                clicked = false
            }
        });
    })();

};

export default Portfolio;

