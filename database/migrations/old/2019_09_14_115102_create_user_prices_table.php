<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('price_type_id');
            $table->unsignedBigInteger('user_id');
            $table->integer('price');
            $table->timestamps();

            $table->foreign('price_type_id')->references('id')->on('price_types')
                ->onDelete('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_prices');
    }
}
