<?php
namespace App\Repositories;

use App\Models\User;

class SeekerRepository extends CommonRepository
{
    protected $model;

    // Constructor to bind model to repo
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    // Get all instances of model
    public function get(Array $params = [], $paginate = false, $query = null)
    {
        $query = $this->model->seekers();

        return Parent::get($params, $paginate, $query);
    }

    // create a new record in the database
    public function create(Array $params)
    {

    }

    // update record in the database
    public function update(Array $params, $slug)
    {

    }

    public function bySlug($slug) {
        return $this->model->byUsername($slug);
    }

    // remove record from the database
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    // show the record with the given id
    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }

    public function first($id)
    {
        return $this->model->findOrFail($id);
    }
}
