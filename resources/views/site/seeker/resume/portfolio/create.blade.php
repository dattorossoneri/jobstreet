<form id="portfolioFormStore" action="{{route('seeker.resume.portfolio.store')}}">
    @include('site.seeker.resume.portfolio.partials.form')
</form>

@include('site.seeker.resume.portfolio.partials.scripts')
