<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('cities')->truncate();

        $data = [
            'es' => [
                "Tallinn", "Tartu", "Narva", "Pärnu", "Kohtla-Järve", "Viljandi", "Rakvere", "Maardu", "Sillamäe", "Kuressaare", "Valga", "Võru", "Jõhvi", "Haapsalu", "Keila", "Paide", "Saue", "Elva", "Põlva", "Tapa"
            ],
            'lv' => [
                "Riga ", "Daugavpils", "Liepāja ", "Jelgava", "Jūrmala ", "Ventspils ", "Rēzekne", "Jēkabpils ", "Valmiera ", "Ogre"
            ],
            'lt' => [
                "Vilnius", "Kaunas", "Klaipėda", "Šiauliai", "Panevėžys", "Alytus", "Marijampolė", "Mažeikiai", "Jonava", "Utena", "Kėdainiai", "Telšiai", "Tauragė", "Ukmergė", "Visaginas", "Radviliškis", "Plungė", "Kretinga", "Palanga", "Druskininkai"
            ],
            'kz' =>[
                "Almaty", "Astana", "Shymkent", "Karaganda", "Taraz", "Aktobe", "Pavlodar", "Oskemen", "Semey", "Oral"
            ],
            'kg' => [
                "Bishkek", "Osh", "Jalal-Abad", "Karakol", "Tokmok", "Kara-Balta", "Uzgen", "Balykchy", "Naryn", "Talas, Kyrgyzstan"
            ],
            'tj' => [
                "Dushanbe", "Khujand", "Kulob", "Qurghonteppa", "Istaravshan", "Vahdat", "Konibodom", "Tursunzoda", "Isfara", "Panjakent", "Khorugh", "Yovon", "Hisor", "Norak", "Farkhor", "Vose'", "Chkalovsk", "Hamadoni", "Danghara", "Somoniyon"
            ],
            'tm' => [
                "Biggest Cities in Turkmenistan", "Ashgabat", "Türkmenabat", "Daşoguz", "Mary", "Balkanabat", "Baýramaly", "Türkmenbaşy", "Tejen", "Abadan", "Magdanly"
            ],
            'uz' => [
                "Tashkent", "Samarkand", "Namangan", "Andijan", "Bukhara", "Nukus", "Qarshi", "Ferghana", "Jizzakh", "Navoiy"
            ],
            'by' => [
                "Minsk", "Homyel", "Mahilyow", "Vitsyebsk", "Hrodna/Grodno", "Brest", "Babruysk", "Baranavichy", "Barysaw", "Pinsk"
            ],
            'md' => [
                'Chișinău','Tiraspol','Bălți'
            ],
            'ua' => [
                "Kiev", "Kharkiv", "Odessa", "Dnipro", "Donetsk", "Zaporizhia", "Lviv", "Kryvyi Rih", "Mykolaiv", "Mariupol"
            ],
            'am' => [
                "Yerevan", "Gyumri", "Vanadzor", "Vagharshapat↵(Etchmiadzin)", "Abovyan", "Kapan", "Hrazdan", "Armavir", "Artashat", "Ijevan", "Charentsavan", "Masis", "Goris", "Ararat", "Gavar", "Sevan", "Artik", "Ashtarak", "Dilijan", "Sisian", "Alaverdi", "Spitak", "Stepanavan", "Vardenis", "Martuni", "Yeghvard", "Vedi", "Nor Hachn", "Byureghavan", "Metsamor", "Berd", "Yeghegnadzor", "Tashir", "Kajaran", "Aparan", "Vayk", "Chambarak", "Maralik", "Noyemberyan", "Talin", "Meghri", "Jermuk", "Agarak", "Ayrum", "Akhtala", "Tumanyan", "Tsaghkadzor", "Shamlugh", "Dastakert"
            ],
            'az' => [
                "Baku", "Ganja", "Sumqayıt", "Lankaran", "Mingelchaur"
            ],
            'ge' => [
                "Tbilisi", "Kutaisi", "Batumi", "Rustavi", "Sukhumi", "Zugdidi", "Gori", "Poti", "Tskhinvali", "Samtredia"
            ],
            'ru' => [
                "Moscow", "St. Petersburg", "Novosibirsk", "Yekateringburg", "Nizhny Novgorod", "Samara", "Omsk", "Kazan", "Chelyabinsk", "Rostov-on-Don", "Ufa", "Volgograd", "Perm", "Krasnoyarsk", "Voronezh", "Saratov", "Krasnodar", "Tolyatti", "Izhhevsk", "Ulyanovsk"
            ]
        ];

        foreach($data as $iso => $cities) {
            foreach($cities as $city) {
                if($country = \App\Models\Country::where('iso',$iso)->first()) {
                    \App\Models\City::create([
                        'country_id' => $country->id,
                        'title' => $city,
                        'slug' => sluggable($city),
                        'position' => 0,
                        'hidden' => 0
                    ]);
                }
            }
        }
    }
}
