import education from './education'
import basicInfo from './basicInfo'
import workExperience from './workExperience'
import portfolio from './portfolio'

export {
    education,
    basicInfo,
    workExperience,
    portfolio
}
