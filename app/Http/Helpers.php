<?php

function sluggable($text) {
    // replace non letter or digits by -
    $slug = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    $slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);

    // remove unwanted characters
    $slug = preg_replace('~[^-\w]+~', '', $slug);

    // trim
    $slug = trim($slug, '-');

    // remove duplicate -
    $slug = preg_replace('~-+~', '-', $slug);

    // lowercase
    $slug = strtolower($slug);

    if (empty($slug)) {
        return 'n-a';
    }

    return $slug;
}


function dateWithoutTime($dateTime) {
    $splited = explode(' ', $dateTime);

    return $splited[0];
}

function dateForHumans($date) {
    $date = \Carbon\Carbon::parse($date);

    return $date->format('d F Y');
}

function collectionToArray($collection, $column) {
    return $collection->pluck($column)->toArray();
}

function getSocialNetwork($socialNetwork, $userSocialNetworks) {
    $arr = collectionToArray($userSocialNetworks,'social_network_id');

    if(in_array($socialNetwork,$arr)) {
        return $userSocialNetworks->where('social_network_id',$socialNetwork)->first()->value;
    }else {
        return '';
    }
}
