<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobLikes extends Model
{
    protected $table = 'job_likes';

    protected $guarded = [];

    public function job() {
        return $this->belongsTo(Job::class, 'job_id', 'id');
    }

    public static function scopeLiked($query, $id) {
        $q = $query->where('job_id', $id);

        if(auth()->check()) {
            $q->where('user_id',auth()->user()->id);
        }

        return $q->count();
    }
}
