<?php

namespace App\Http\Middleware;

use Closure;

class RecruiterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth()->user()->isRecruiter()) {
            abort(403);
        }
        return $next($request);
    }
}
