<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobTypes extends Model
{
    protected $table = 'job_types';

    protected $guarded = [];

    public function jobs()
    {
        return $this->belongsToMany(Job::class, 'job_types_job');
    }

}
