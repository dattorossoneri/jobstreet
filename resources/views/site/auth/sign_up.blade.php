@extends('site.auth.layout')
@section('content')
    @push('css')
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    @endpush
    <main id="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form action="" id="signUpForm" method="post">
                        {!! csrf_field() !!}
                        <div class="panel panel-default">
                            <div class="panel-heading">Sign Up</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="user_type">Profile Type</label>
                                    <select name="user_type" id="user_type" class="form-control">
                                        <option value="">Choose</option>
                                        @foreach($userTypes as $userType)
                                            <option value="{{$userType->id}}">{{$userType->title}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">First Name</label>
                                            <input type="text" class="form-control" name="name" id="name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="last_name">Last Name</label>
                                            <input type="text" class="form-control" name="surname" id="last_name">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="country">Country</label>
                                            <select data-live-search="true" data-url="{{route('cities.get',['__id__'])}}" name="country" id="country" class="form-control">
                                                <option value="">Choose Country</option>
                                                @foreach($countries as $country)
                                                    <option {{ (!empty($record) and $record->country_id == $country->id) ? 'selected' : '' }} value="{{$country->id}}">
                                                        {{$country->title}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="city">City</label>
                                            <select class="form-control" name="city" id="city">
                                                <option value="">Choose</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email">E-Mail</label>
                                    <input type="email" class="form-control" name="email" id="email">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" name="password" id="password">
                                </div>
                                <div class="form-group">
                                    <label for="confirm_password">Confirm Password</label>
                                    <input type="password" class="form-control" name="password_confirmation" id="confirm_password">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="terms"> I agree <a href="#">terms and conditions</a>
                                    </label>
                                </div>
                                <hr>
                                <button disabled type="submit" class="btn btn-primary register">Sign Up</button>
                                <a href="{{route('sign_in')}}" class="btn btn-link">Do you have an account?</a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </main>
    @push('js')
        <script src="{{asset('dist/js/popper.min.js')}}" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        {{--<script src="{{asset('dist/js/select.min.js')}}"></script>--}}
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
        <script src="{{asset('js/SignUp.js')}}"></script>
    @endpush
@endsection

