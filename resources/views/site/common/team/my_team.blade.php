@if(!empty($teams))
    <table class="table table-bordered table-hover">
        <thead>
        <tr class="active">
            <th>Team Name</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <tbody>
            @foreach($teams as $myTeam)
                <tr>
                    <td><a href="#">{{$myTeam->title}}</a></td>
                    <td class="text-center">
                        <a href="javascript:" data-url="{{ route('team.delete', $myTeam->id) }}" class="btn btn-danger btn-xs delete-team">
                            <i class="fa fa-times" aria-hidden="true"></i>
                            Delete
                        </a>
                        <a href="#" class="btn btn-info btn-xs">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            Members
                        </a>
                    </td>

                </tr>
            @endforeach
        </tbody>
    </table>
@else
    <h5 class="text-center">
        You have not teams
    </h5>
@endif
