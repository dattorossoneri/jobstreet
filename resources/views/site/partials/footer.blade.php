<footer>
    <div class="container">
        <div class="row equal">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <h5>COMPANY</h5>
                <ul class="list-unstyled quick-links">
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> About Us</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Investor Relations</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Careers</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Press</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Trust & Safety</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Terms of Service</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Privacy Policy</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Accessibility</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <h5>RESOURCES</h5>
                <ul class="list-unstyled quick-links">
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Customer Support</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Hiring Headquarters</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Hiring Resources</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Terminalio Blog</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Customer Stories</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Business Resources</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Payroll Services</a></li>
                </ul>
            </div>
            <div class="clearfix visible-sm-block"></div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <h5>BROWSE</h5>
                <ul class="list-unstyled quick-links">
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Freelancers by Skill</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Freelancers in USA</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Freelancers in UK</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Freelancers in Canada</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Freelancers in Australia</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Jobs in USA</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Find Jobs</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <h5>Languages</h5>
                <ul class="list-unstyled quick-links">
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> English</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Русский</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-angle-double-right"></i> Українська</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <ul class="list-unstyled list-inline text-center">
                    <li class="list-inline-item"><a href="javascript:void(0);"><i class="fa fa-linkedin-square fa-2x" aria-hidden="true"></i></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0);"><i class="fa fa-facebook-official fa-2x" aria-hidden="true"></i></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0);"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0);"><i class="fa fa-google-plus-square fa-2x" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-center">
                <p>Terminalio Global Inc &copy 2019</p>
            </div>
        </div>
    </div>
</footer>
