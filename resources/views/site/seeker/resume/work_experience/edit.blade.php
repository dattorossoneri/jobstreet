<form id="workExperienceFormUpdate" action="{{route('seeker.resume.work_experience.update',[$item->id])}}">
    @include('site.seeker.resume.work_experience.partials.form')
</form>

@include('site.seeker.resume.work_experience.partials.scripts')
