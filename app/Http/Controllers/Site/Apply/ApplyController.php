<?php

namespace App\Http\Controllers\Site\Apply;

use App\Actions\MakeApply;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;

class ApplyController extends Controller
{
    public function send(Request $request, MakeApply $action) {
        return $action->execute($request->all());
    }
}
