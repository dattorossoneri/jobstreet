<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('countries')->truncate();

        $countries = [
            [
                'title' => 'Estonia',
                'position' => 0,
                'iso' => 'es',
                'hidden' => 0
            ],

            [
                'title' => 'Russia',
                'position' => 0,
                'iso' => 'ru',
                'hidden' => 0
            ]
        ];

        foreach($countries as $country) {
            $country['slug'] = sluggable($country['title']);
            \App\Models\Country::create($country);
        }
    }
}
