<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Step 2 - Education</div>
            <div class="panel-body" data-url="{{route('seeker.resume.education.create')}}" id="education-content">

            </div>
        </div>
    </div>
    <div data-url="{{route('seeker.resume.education.list')}}" id="education-list">

    </div>
</div>

