<?php
namespace App\Repositories;

use App\Helpers\JobTypeHelper;
use App\Helpers\TechnologyHelper;
use App\Models\Job;
use App\Models\JobTypes;
use App\Models\Technology;
use App\Repositories\RepositoryInterface as Repository;
use Carbon\Carbon;

class JobRepository implements Repository
{
    protected $request;
    protected $model;
    protected $user;

    protected $filterParams = [
        'page',
        'user_id',
        'keyword',
        'country',
        'city',
        'job_types',
        'date',
        'technologies'
    ];

    // Constructor to bind model to repo
    public function __construct()
    {
        $this->model = new Job();
    }

    // Get all instances of model
    public function get(Array $params = [], $paginate = false, $query = null)
    {

        $query = $this->model->newQuery();

        $this->filterParams($params);

        if(!empty($params)) {
            if( isset($params['user_id']) and $value = $params['user_id']) {
                $query->where('user_id',$value);
            }

            if( isset($params['keyword']) and $value = $params['keyword']) {
                $query->where('title','LIKE',"%".$value."%");
            }

            if( isset($params['country']) and $value = $params['country']) {
                $query->where('country_id',$value);
            }

            if( isset($params['city']) and $value = $params['city']) {
                $query->where('city_id',$value);
            }


            if(isset($params['job_types']) and $values = $params['job_types']) {
                $query->whereHas('jobTypes', function($q) use($values) {
                    $q->whereIn('job_types_id',$values);
                });
            }

            if(isset($params['technologies']) and $values = $params['technologies'] and $values != '[]') {

                $technologies = (new TechnologyHelper())->parse($values);
                $query->whereHas('technologies', function($q) use($technologies) {
                    $q->whereIn('technology_id',$technologies);
                });

            }

            if($value = request()->get('job_date')) {
                $date = null;
                switch ($value) {
                    case 1:
                        $date = Carbon::now()->addDay(-15);
                        break;

                    case 2:
                        $date = Carbon::now()->addDay(-7);
                        break;

                    case 3:
                        $date = Carbon::now()->addDay(-3);
                        break;

                    case 4:
                        $date = Carbon::now()->addDay(-1);
                        break;

                    default:
                        break;

//                case 5:
//                    $date = auth()->user()->last_search_date;
//                break;
                }

                $query->whereDate('published_at', '>=', $date);
            }
        }

        if ($paginate) {
            return $query->orderBy('created_at','desc')->paginate($paginate);
        }

        if($query)
            return $query;

        return $query->get();
    }


    protected function filterParams($params) {
        foreach($params as $key => $value) {
            if(!in_array($key,$this->filterParams)) {
                abort(403);
            }
        }
    }


    // create a new record in the database
    public function create(Array $params){}

    // update record in the database
    public function update(Array $params, $slug){}

    public function bySlug($slug) {
        return $this->model->bySlug($slug);
    }

    // remove record from the database
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    // show the record with the given id
    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }

    public function first($id)
    {
        return $this->model->findOrFail($id);
    }
}
