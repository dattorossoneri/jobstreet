@extends('site.auth.layout')
@section('content')
    <main id="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form action="" id="signIn" method="post">
                        {!! csrf_field() !!}
                        <div class="panel panel-default">
                            <div class="panel-heading">Sign In</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="email">E-Mail</label>
                                    <input type="email" class="form-control" name="email" id="email">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" name="password" id="password">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> Remember Me
                                    </label>
                                </div>
                                <hr>
                                <button type="submit" class="btn btn-primary">Log in</button>
                                <a href="forgot.html" class="btn btn-link">Forgot password?</a>
                            </div>
                        </div>
                    </form>

                    <div class="alert alert-warning" role="alert">
                        New user? <a href="{{route('sign_up')}}">Sign Up</a>
                    </div>
                </div>
            </div>
        </div>
    </main>
    @push('js')
        <script src="{{asset('js/signIn.js')}}"></script>
    @endpush
@endsection

