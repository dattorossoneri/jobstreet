<?php
namespace App\Repositories;

use App\Models\Country;
use App\Repositories\RepositoryInterface as Repository;

class CountryRepository implements Repository
{
    protected $model;

    // Constructor to bind model to repo
    public function __construct()
    {
        $this->model = (new Country());
    }

    // Get all instances of model
    public function get(Array $params = [], $paginate = false, $query = null)
    {
        if ($paginate) {
            return $this->model->paginate($paginate);
        }

        return $this->model->orderBy('position','desc')->get();
    }

    // create a new record in the database
    public function create(Array $params){}

    // update record in the database
    public function update(Array $params, $slug){}

    // remove record from the database
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    // show the record with the given id
    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }

    public function first($id)
    {
        return $this->model->findOrFail($id);
    }
}
