<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate();

        $users = [
            [
                'name' => 'Levani',
                'surname' => 'Soroznishvili',
                'username' => 'levani',
                'email' => 'levani@gmail.com',
                'password' => bcrypt('123456'),
                'active' => 1,
                'user_type_id' => 1,
                'country_id' => rand(1,15)
            ],
            [
                'name' => 'Andro',
                'surname' => 'Siboshvili',
                'username' => 'andro',
                'email' => 'andro@gmail.com',
                'password' => bcrypt('123456'),
                'active' => 1,
                'user_type_id' => 1,
                'country_id' => rand(1,15)
            ],
            [
                'name' => 'Dato',
                'surname' => 'Liluashvili',
                'username' => 'dato',
                'email' => 'dato@gmail.com',
                'password' => bcrypt('123456'),
                'active' => 1,
                'user_type_id' => 1,
                'country_id' => rand(1,15)
            ],
            [
                'name' => 'John',
                'surname' => 'Doe',
                'username' => 'john_doe',
                'email' => 'john_doe@gmail.com',
                'password' => bcrypt('123456'),
                'active' => 1,
                'user_type_id' => 2,
                'country_id' => rand(1,15)
            ]
        ];

        foreach($users as $user) {
            User::create($user);
        }
    }
}
