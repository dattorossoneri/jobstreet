<?php

namespace App\Http\Controllers\Site\Seeker;

use App\Actions\ChangeOfferStatus;
use App\Http\Controllers\Controller;

class SeekerOfferController extends Controller
{
    public function index() {
        return view('site.seeker.offers.index');
    }

    public function load() {
        $data['offers'] = auth()->user()->receivedOffers;

        return view('site.seeker.offers.loads.offers', $data);
    }

    public function changeStatus($id, ChangeOfferStatus $action) {
        return $action->execute($id, request()->get('status'));
    }

}
