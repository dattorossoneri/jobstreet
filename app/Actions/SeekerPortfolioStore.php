<?php

namespace App\Actions;


use App\Models\UserPortfolio;

class SeekerPortfolioStore {

    protected $model;

    public function __construct(UserPortfolio $model)
    {
        $this->model = $model;
    }

    public function execute(array $data)  {
        return ($this->model->create([
            'user_id' => auth()->user()->id,
            'title' => $data['title'],
            'image' => $this->uploadImage($data['image']),
            'url' => $data['url'],
            'description' => $data['description']
        ])) ? true : false;
    }

    protected function uploadImage($image) {
        if( is_object($image) and get_class($image) === 'Illuminate\Http\UploadedFile') {
            $user = auth()->user();
            $dir = public_path("portfolio/$user->username");
            $extension = $image->getClientOriginalExtension();
            $fileName = md5($user->username).rand(999,9999).time().".".$extension;

            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }

            $image->move($dir, $fileName);

            return url("portfolio/$user->username/$fileName");
        }else {
            abort(500);
        }
    }
}
