<?php

namespace App\Http\Controllers\Site\Seeker\Resume;

use App\Actions\UpdateUserBasicInfo;
use App\Http\Requests\UserBasicInfoRequest;
use App\Models\City;
use App\Models\Country;
use App\Http\Controllers\Controller;
use App\Models\EnglishLevel;

class SeekerBasicInfoController extends Controller
{
    public function index() {
        $data['countries'] = Country::get();
        $data['cities'] = City::get();
        $data['user'] = auth()->user();
        $data['englishLevels'] = EnglishLevel::get();

        return view('site.seeker.resume.basic.index', $data);
    }

    public function update(UserBasicInfoRequest $request, UpdateUserBasicInfo $action) {
        return response()->json($action->execute($request->All()), 200);
    }
}
