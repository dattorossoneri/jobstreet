@extends('site.layout')
@section('content')
    <div class="col-lg-9 col-md-9 col-sm-8">
        <form id="jobEditForm" action="{{route('recruiter_job.update',[$record->slug])}}" method="post">
            @include('site.recruiter.jobs.includes.form')
        </form>
    </div>
@endsection
