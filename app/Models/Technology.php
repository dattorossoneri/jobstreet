<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Technology extends Model
{
    protected $table = 'technologies';

    protected  $guarded = [];

    public function jobs()
    {
        return $this->belongsToMany(Job::class, 'job_technology');
    }

    public function employees() {
        return $this->belongsToMany(User::class, 'user_technology','technology_id');
    }
}
