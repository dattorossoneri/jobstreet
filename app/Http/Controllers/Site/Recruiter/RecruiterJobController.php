<?php

namespace App\Http\Controllers\Site\Recruiter;


use App\Actions\UpdateJob;
use App\Models\Country;
use App\Models\JobTypes;
use App\Models\Technology;
use App\Actions\CreateJob;
use App\Http\Requests\JobRequest;
use App\Repositories\JobRepository;
use App\Http\Controllers\Controller;

class RecruiterJobController extends Controller
{
    protected $repo;

    public function __construct(JobRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index() {
        $data['jobs'] = $this->repo->get(['user_id' => auth()->user()->id],20);

        return view('site.recruiter.jobs.index',$data);
    }

    public function create() {
        $data['countries'] = Country::get();
        $data['technologies'] = Technology::get();
        $data['jobTypes'] = JobTypes::get();

        return view('site.recruiter.jobs.create', $data);
    }

    public function store(JobRequest $request, CreateJob $action) {
        if($record = $action->execute($request->all())) {
            return response()->json([
                'url' => route('recruiter_job.edit',$record->slug)
            ],201);
        }

        abort(500);
    }

    public function edit($slug) {
        $record = $this->repo->bySlug($slug);

        $data['countries'] = Country::get();
        $data['technologies'] = Technology::get();
        $data['jobTypes'] = JobTypes::get();

        $data['choiceTechnologies'] = implode(',',collectionToArray($record->technologies, 'id'));
        $data['choiceJobTypes'] = $record->jobTypes->pluck('id')->toArray();

        $data['record'] = $record;

        return view('site.recruiter.jobs.edit', $data);
    }

    public function update(JobRequest $request,$slug, UpdateJob $action) {
        if($record = $action->execute($slug,$request->all())) {
            return response()->json([
                'url' => route('recruiter_job.edit', $record->slug)
            ],200);
        }
    }

    public function delete($slug) {

        if($this->repo->bySlug($slug)->delete()) {
            return response()->json('deleted',200);
        }

        abort(500);
    }
}
