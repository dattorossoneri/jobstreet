<div class="form-group">
    <label for="title">Job Title</label>
    <input type="text" class="form-control" value="{{ !empty($item) ? $item->title : '' }}" name="title" id="title">
</div>

<div class="form-group">
    <label for="company">Company</label>
    <input type="text" class="form-control" value="{{ !empty($item) ? $item->company_name : '' }}" name="company" id="company">
</div>

<div class="form-group">
    <label for="school">Description</label>
    <textarea name="description" id="" cols="30" rows="3" class="form-control">{!! !empty($item) ? $item->description : '' !!}</textarea>
    <p class="help-block">Describe your position and any significant accomplishments.</p>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="country">Country</label>
            <select name="country" data-url="{{route('cities.get',['__id__'])}}" data-live-search="true" id="country" class="form-control required">
                <option value="">Choose</option>
                @foreach($countries as $country)
                    <option
                        {{  (!empty($item) and $item->country_id === $country->id)  ? 'selected' : '' }}
                        value="{{$country->id}}">
                        {{$country->title}}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="city">City</label>
            <select data-live-search="true" name="city" id="city" class="form-control required">
                @if(!empty($item))
                    @foreach($item->country->cities as $city)
                        <option
                            {{  ($city->id === $item->city_id)  ? 'selected' : '' }}
                            value="{{$city->id}}">
                            {{$city->title}}
                        </option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="from">From</label>
            <input autocomplete="off" value="{{ (!empty($item)) ? $item->from : '' }}" type="text" class="form-control required datepicker" name="from" id="from">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="to">To</label>
            <input autocomplete="off" value="{{ (!empty($item) and !empty($item->to)) ? $item->to : '' }}" type="text" class="form-control required datepicker" name="to" id="to">
        </div>
    </div>

    <div class="col-md-6">
        <div class="checkbox">
            <label>
                <input name="till" {{ (!empty($item) and $item->till) ? $item->till : '' }} class="required" type="checkbox"> I currently go here
            </label>
        </div>
    </div>
</div>

<button type="submit" class="btn btn-success btn-sm">
    <i class="fa fa-save"></i> Save
</button>
